#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Authors : Jocelyn Etienne
# (CC) july 2023 -- present
# =============================================================================

"""
This class handles warnings and logging
"""

def logger_normal_mode():
    return { 'on_screen': 1, 'in_logfiles':3,
             'numerics': 3, 'physics':3, 'algorithmics':2,'user':3,
             'check_numerics': 0, 'check_physics':0, 'check_algorithmics':0,
             }

def logger_quiet_mode():
    return { 'on_screen': 0, 'in_logfiles':3,
             'numerics': 3, 'physics':3, 'algorithmics':2,'user':3,
             'check_numerics': 0, 'check_physics':0, 'check_algorithmics':0,
             }


def logger_debug_mode():
    return { 'on_screen': 5, 'in_logfiles':5,
             'numerics': 5, 'physics':5, 'algorithmics':5, 'user':5,
             'check_numerics': 5, 'check_physics':5, 'check_algorithmics':5,
           }

class Logger(object):
    def __init__(self, parameters):
        """
        Use e.g. logger_normal_mode() to provide default parameters.  Logging
        on screen and in logfiles will be limited to messages of level lower or
        equal than the minimum of the thematic level ('numerics', 'physics', 
        'user', or 'algorithmics') and the output type level ('on screen' or
        'in logfiles').
        User classes can also perform additional checks if 'check_*' levels
        are set.
        """
        self.par = parameters
    def algorithmics(self, level, *message):
        if (level<=self.par['algorithmics']): 
            self.on_screen(level, message)
            self.in_logfiles(level, message)
    def physics(self, level, *message):
        if (level<=self.par['physics']): 
            self.on_screen(level, message)
            self.in_logfiles(level, message)
    def numerics(self, level, *message):
        if (level<=self.par['numerics']): 
            self.on_screen(level, message)
            self.in_logfiles(level, message)
    def user(self, level, *message):
        if (level<=self.par['user']): 
            self.on_screen(level, message)
            self.in_logfiles(level, message)
    def on_screen(self, level, message):
        if (level<=self.par['on_screen']): print(level*" |", *message)
    def in_logfiles(self, level,message):
        if (level<=self.par['in_logfiles']*0): print(message)
