#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

"""
Test case: vertical Maxwell fluid thread submitted to a step stress at its end
---
Will validate:
* analytical comparison for viscoelastic relaxation
"""



import sys,os
# allow to find classes in the parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from class_cortex import *
from common_utils import *
import time
import datetime
try:
    import cpuinfo
    mycpuinfo = cpuinfo.get_cpu_info()['brand_raw']
except:
    mycpuinfo = "Unknown CPU -- install py-cpuinfo to record it"


def clamped_bc(psiup):
    """
        x_a=0, y_a=L0, θ_a=-π/2
    """
    global L0
    return [get_x(psiup), get_y(psiup)-L0, get_theta(psiup)+np.pi/2]

def load_bc(psidown):
    """
       E(α²-1) = ΕF_0 at S=1, and no torque:
       α² = F_0 + 1, θ_a'=0, θ_a''=0
    """
    global F0
    return [get_alpha(psidown)-1-F0, get_curvature(psidown), get_diff_curvature(psidown)]

# Tolerance of non-regression
tol=1e-2
# Set to False to run test.
# Set to True to regenerate non-regression test results. /!\
build_test=False
if build_test:
    yes = input("Confirm recalculation of nonregression test results. yes/[no]:")
    if yes!='yes':
        print("Running as a test, remember to turn build_test=False in the code")
        build_test=False

kappa=1 # indifferent
L0 = 1.
# will be zipped 
F0_list =          [ .05, .05, .05,  .5  ]   # force applied from t=0 to t=t1
t1_list =          [ 10., 1.,  1.,   1.  ]
inv_tau_c_list=    [ 300, 30,  3,    300 ] # inverse of the relaxation time
Nsteps_list=       [ 80,  800, 8000, 80  ]

F0=case['F0'] # will be made 0 at t1

if build_test:
    cases = []
    for F0,t1,inv_tau,Nsteps in zip([F0_list,t1_list,inv_tau_c_list,Nsteps_list]):
            cases += [ { 'F0':F0, 't1':t1, 'inv_tau_c':inv_tau_c_list, 'Nsteps':Nsteps,
                         'cpu': mycpuinfo, 'date':str(datetime.datetime.today()) } ]
else:
    cases = np.load("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], allow_pickle=True)

perf=[]

cortex_par=cortex_par_defaults()
cx.cortex_par["kappa"]=kappa
if case in cases:
    cortex_par["elastic_tolerance"]=1e-3
    cortex_par["boundary_conditions"]='custom'
    cortex_par["inv_tau_c"]=case['inv_tau_c']
    cx = Cortex({'shape':'segment', 'from':(0,L0), 'to':(0,0), 'N':100}, cortex_par)
    cx.set_custom_separate_bc(clamped_bc,load_bc)

    F0 = case['F0']
    Nsteps=case['Nsteps']
    t1=case['t1']
    tfinal=1.2*t1
    trange=np.append(np.linspace(0,t1,Nsteps),np.linspace(t1,tfinal,Nsteps//5)[1:])
    deflection=[0]
    tprev=-tfinal/Nsteps

    cpu_t0=time.process_time()
    for t in trange:
        if t>t1: F0=0
        res=cx.elastic_equilibrium()
        cx.relax(t-tprev)
        tprev=t
        deflection += [ -cx.y()[-1] ]
        if res!=0: print("No convergence, exiting"); exit(1)
    cputime=time.process_time()-t0
    success_status = res 

    Theta=1
    Theta1=1
    E=.5
    F0=case['F0']
    theory=[0] \
        +list( L0*( (1+F0/(2*E+Theta*F0))*np.exp(inv_tau_c*F0*np.linspace(0,t1,Nsteps)/(2*E+Theta*F0)) - 1 ) ) \
        +list( L0*( 1-F0/(2*E+Theta1*F0) )*( (1+F0/(2*E+Theta*F0))*np.exp(inv_tau_c*F0*np.linspace(t1,t1,Nsteps//5)[1:]/(2*E+Theta*F0)) ) - L0 ) 

    if res==0:
        max_error=np.max(np.abs(np.array(theory)-np.array(deflection)))
    if build_test:
        if res==0:
            case['max_error']=max_error
            case['cputime']=cputime
            case['success']=True
        else:
            case['success']=False
        print(case)
    else:
        fail=False
        if res>0 and case['success']:
            print ("Not converging when it used to.\n",case)
            exit(1)
        if case['success']:
            if max_error-case['max_error'] > tol*case['max_error']: 
                print ("Not within tolerance.\n",case)
                exit(1)
            perf+=[ cputime/case['cputime'] ]


if build_test:
    np.save("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], cases)
else:
    print("Success with median relative performance ", np.median(perf))
    if np.median(perf)>1.1:
       print("Performance unexpectedly poor, marked as failed")
       exit(1)
