% main: main.tex

\newcommand{\kaptwist}{\kappa_{\mathrm{twist}}}

\chapter[Morphoelastic rods]{Modelling of morphoelastic Kirchhoff rods}

\section{Deformation of initially straight rods}

We take inspiration from the approach of \cite{Audoly-Pomeau.2010.1} for Kirchhoff rods, but use
a morphoelastic multiplicative decomposition of the gradient of deformation tensor, associated with the
strain energy derived in \cite{Jallon+Etienne.2024.1}.

Our objective here is to determine a constitutive relation between the elastic part of the longitudinal stretch, $\alpha(S)$,
and the \emph{internal force} or tension $T(S)$, for a given \emph{anelastic pre-stretch} $\vc{\gamma}(S)$.
\JE[All these quantities are defined below.]{integrate in rest of doc}

\subsection{Kinematics}

We consider what will be called the \emph{primary configuration} of a slender elastic body.
Its \emph{centreline} is a straight line contained in the plane $z=0$,
$\vc{R}_c(S) = \vc{R}_0 + {S}\vc{D}_0$,
for ${S} \in [0,{L}]$ where $\vc{R}_0 \in \Reels^2$. Material points position can be described using the orthonormal frame
where $\vc{D}_0 = \dedl{\vc{R}_c}{S}$ is complemented by $\vc{D}_2 = \vc{e}_z$ and $\vc{D}_1 = - \vc{D}_0 \times \vc{D}_2$,
so that:
$$
{\pt{X}}({S},\Delta_1,\Delta_2) = {\pt{R}}_c({S}) + \Delta_1 {\vc{D}}_1({S}) + \Delta_2 \vc{e}_z
$$
for ${S} \in [0,{L}]$, $\Delta_1,\Delta_2 \in \mathcal{S}({S})$, with $\mathcal{S} \in \mathcal{B}_2(W)$ the
ball of $\Reels^2$ of radius $W \ll L$.
\JE{Can $\mathcal{S}({S})$?}

Let $\ts{F}$ be the deformation tensor.
The unshearable rods hypothesis states:
\JE{Is that it?}
\begin{hypothesis}[Euler--Bernoulli]
\label{hyp-EulerBernoulli}
% $(\vc{d}_0,\vc{d}_1,\vc{d}_2) = (ts{F d_i)$
There exists a local frame $(\vc{d}_0,\vc{d}_1,\vc{d}_2)$
\JE[where $\vc{d}_0$ is a function of $S,\Delta_1,\Delta_2$ and
$\vc{d}_2,\vc{d}_3$ depend on $S$ only]{to resolve the locking of thickness...}
such that 
the deformation tensor can be written:
$$\ts{F} = \lambda_0\vc{d}_0\vc{D}_0 + \lambda_1\vc{d}_1\vc{D}_1 + \lambda_2\vc{d}_2\vc{D}_2.$$
where $\lambda_i(S,\Delta_1,\Delta_2)$, the stretch coefficients along directions $i \in \{0,1,2\}$, are
of order 1 and vary smoothly such that $\dedl{\lambda_i}{S} = O(1/L)$.
\end{hypothesis}
Note that the literature usually 
denotes $\vc{d}_3$ the tangential vector here denoted $\vc{d}_0$, the latter notation being convenient to
keep the mathematical development and numerical implementation consistent.

Then, $\mathrm{d}\vc{x} = \ts{F}\mathrm{d}\vc{X} 
= \lambda_0 \vc{d}_0 \mathrm{d}S + \lambda_1 \vc{d}_1 \mathrm{d}\Delta_1 + \lambda_2 \vc{d}_2 \mathrm{d}\Delta_2$
and specifically $\mathrm{d}\vc{r}_c := \ts{F}\mathrm{d}\vc{R}_c = \lambda_0 \vc{d}_0 \mathrm{d}S$ can
be integrated to give
$\pt{r}_c(S) = \pt{r}_0 + \intd{0}{S}{ \lambda \vc{d}_0 }{S}$, the curve described by the centreline in the 
current configuration. Here $\vc{r}_0$ is a so far unknown integration constant, and $\lambda(S) := \lambda_0(S,\Delta_1=\Delta_2=0)$
is the unknown stretch of the centreline.
Denoting $\vc{d}(S)=\vc{d}_0(S,\Delta_1,\Delta_2)$, 
it is worth to be noted that, then, the curvilinear coordinate along the centreline is $s$ such that $\dedl{s}{S} = \lambda$
and that as a result, $\dedl{\vc{d}}{S} = \lambda \vc{\kappa}$ where $\vc{\kappa}$ is the local Frenet curvature vector
of the centreline in its current configuration, with $\vc{\kappa}\cdot\vc{d} = 0$.

Integrating now relative to $\Delta_i$ only, we 
give the current configuration:
$$
\pt{x}(S,\Delta_1,\Delta_2) = \pt{r}_c(S) + \delta_1 \vc{d}_1(S) + \delta_2 \vc{d}_2,
$$
where $\delta_i(S,\Delta_1,\Delta_2) = \intd{0}{\Delta_i}{ \lambda_i(S,\Delta_1,\Delta_2) }{\Delta_i}$.
Lines at constant offset in the reference configuration are $\pt{r}_c + \sum_i \delta_i(S,\Delta_1,\Delta_2)\vc{d}_i$
and thus have for tangent 
$$
\vc{d}_0(S,\Delta_1,\Delta_2) = \frac{\theta}{\lambda} \left( \lambda \vc{d}
	+ \sum_{i=1,2}\left(  \ded{\delta_i}{S}(S,\Delta_1,\Delta_2) \vc{d}_i(S) + \delta_i(S,\Delta_1,\Delta_2) \ded{\vc{d}_i}{S}(S) \right)\right)
$$
where $\theta$ is a normalisation constant. We observe that $\dedl{\vc{d}_i}{S} = (-1)^{i'}\lambda\kaptwist \vc{d}_{i'} - \lambda\kappa_i\vc{d}$
where we have introduced the rate of twist $\kaptwist(S)$ and $i'=1$ if $i=2$, respectively $i'=2$ if $i=1$. 
Note that $\kappa_i = \vc{\kappa}\cdot\vc{d}_i$ are the
components of the curvature vector and not of the Darboux vector.

We now make explicit the hypothesis of slenderness of the rod with respect ot both its length and its radius of curvature in the
current configuration.
\begin{hypothesis}[slenderness]
Let $L'=\min\{L,\inf_{S} 1/|\vc{\kappa}(S)|\}$ be a lower bound of the curvature radius, we assume that $\ts{F}$ is such that $W \ll L'$.
\end{hypothesis}
This yields:
$$
\vc{d}_0(S,\Delta_1,\Delta_2) = \theta\left(1-\sum_{\smash{i=1,2}}\kappa_i\delta_i\right)\vc{d}\ 
	+ \theta\sum_{i=1,2} \left( \lambda^{-1}\ded{\delta_i}{S} + (-1)^{i} \delta_{i'}\kaptwist\right) \vc{d}_i 
		 + O\left(\frac{W}{L'}\right)^2
$$
where only $\delta_i$ depends on $\Delta_j$ on the right hand side. 
Additionally, $\delta_i \kappa_i$ and $\delta_i \kaptwist$ are of the order of $W/L' \ll 1$, and using the hypothesis of slenderness and $\dedl{\lambda_i}{S} = O(1/L)$, 
we have $\theta = 1 + \sum_{\smash{i=1,2}}\kappa_i\delta_i + O(W/L')^2$.
$$
\vc{d}_0(S,\Delta_1,\Delta_2) = \vc{d}\ 
	+ \sum_{i=1,2}\left( \lambda^{-1}\ded{\delta_i}{S} + (-1)^{i} \delta_{i'}\kaptwist\right) \vc{d}_i 
		 + O\left(\frac{W}{L'}\right)^2
$$

\JE{At this stage AP10 introduce $\epsilon$ Green-Lagrange strain tensor, I try to bypass this for multiplicative decomposition}

\includesvg[width=.5\textwidth]{deformation_paths_of_integration.svg}

We can however integrate along a different path, consider:
\begin{align*}
&\pt{x}(S+\mathrm{d}S,\Delta_1,\Delta_2)-\pt{x}(S,0,0) \\
 &\quad= \lambda(S) \mathrm{d}S \,\vc{d} + \delta_1(S+\mathrm{d}S,\Delta_1,\Delta_2) \vc{d}_1(S+\mathrm{d}S) + \delta_2(S+\mathrm{d}S,\Delta_1,\Delta_2) \vc{d}_2(S+\mathrm{d}S) + o(\mathrm{d}S)\\
 &\quad= \delta_1(S,\Delta_1,\Delta_2) \vc{d}_1(S)  + \delta_2(S,\Delta_1,\Delta_2) \vc{d}_2 + \lambda_0(S,\Delta_1,\Delta_2)\mathrm{d}S \, \vc{d}_0(S,\Delta_1,\Delta_2) + o(\mathrm{d}S)
\end{align*}
Inserting the above, and using the summation convention on repeated indices over values 1 and 2,
\begin{align*}
&\lambda(S) \mathrm{d}S \,\vc{d} + \delta_i(S+\mathrm{d}S,\Delta_1,\Delta_2)\left( \vc{d}_i(S)+ (-1)^{i'} \lambda\kaptwist\mathrm{d}S \vc{d}_{i'} - \lambda\kappa_i\mathrm{d}S \vc{d} \right)
	+ o(\mathrm{d}S)+O\left(\frac{W}{L'}\right)^2\\
 &\quad= \delta_i(S,\Delta_1,\Delta_2) \vc{d}_i(S) + \lambda_0(S,\Delta_1,\Delta_2)\mathrm{d}S \left(\vc{d} + \left( \lambda^{-1}\ded{\delta_i}{S} + (-1)^{i} \delta_{i'}\kaptwist\right)\vc{d}_i\right) 
\end{align*}

Hence, equating along the three axes:
%$$
%\lambda_0(S,\Delta_1,\Delta_2)\vc{d}_0 
%  = \lambda(S)\vc{d}_0 + \delta_1\ded{\vc{d}_1}{S} + \delta_2\ded{\vc{d}_2}{S}
%    + \ded{\delta_1}{S} \vc{d}_1(S) + \ded{\delta_2}{S} \vc{d}_2(S) + o(1) 
%$$
%\JE{rather:}
%and hence:
\begin{align*}
\lambda_0(S,\Delta_1,\Delta_2)
  =& \lambda(S)(1 - \delta_1 \kappa_1 - \delta_2 \kappa_2) + O\left(\frac{W}{L'}\right)^2
\\
 \ded{\delta_1}{S} =&  O\left(\frac{W}{L'}\right)
\\
 \ded{\delta_2}{S} =&  O\left(\frac{W}{L'}\right)
\end{align*}
which must be enforced on $(\lambda_i)_i$ and $(\vc{d}_i)_i$ so as to have an admissible transformation $\vc{F}$.
\JE[One first result is that the thickness of the rod cannot vary on the relevant
length scale $L'$, since $\delta_i$ are constant over that length scale, implying that $\lambda_1,\lambda_2$ depend 
on $\Delta_1,\Delta_2$ only. ]{Not true! Would need to go to next order...}
%\JE[The second and third relation 

Here we make an additional assumption to restrict ourselves to the special case when rods are not twisted.
\begin{hypothesis}[Alignment of the principal directions of the rod section]
Assume $\vc{d}_2 \equiv \vc{e}_z$, and that for all $S$, $\iintd{\mathcal{S}(S)}{}{ \delta_1\delta_2 }{\Delta_1}{\Delta_2} = 0$.
\JE{?}
\end{hypothesis}
Then it follows immediately from \refhyp{hyp-EulerBernoulli} that $\vc{d}_1 = \vc{e}_z \times \vc{d}_0$ and
hence that $\dedl{\vc{d}_1}{S} = - \lambda \kappa \vc{d}_0$ where $\kappa(S)$ is the signed curvature
$\kappa = \vc{\kappa}\cdot\vc{d}_1$. With this, we find that the above consistency condition can be written
as:
$$
\lambda_0(S,\Delta_1,\Delta_2) = (1 - \delta_1(\Delta_1,\Delta_2)\kappa(S))\lambda(S).
$$

\subsection{Morphoelastic decomposition of the deformation gradient}

We decompose the deformation gradient,
\begin{align*}
%\ts{F}(S,\Delta_1,\Delta_2) =  \lambda_0 \vc{d}_0\vc{D}_0 + \lambda_1\vc{d}_1\vc{D}_1 + \lambda_2\vc{d}_2\vc{D}_2,
\ts{F}(S,\Delta_1,\Delta_2) =&  \lambda_0(S,\Delta_1,\Delta_2) \vc{d}_0(S,\Delta_1,\Delta_2)\vc{D}_0 
	\\&
	+ \lambda_1(S,\Delta_1,\Delta_2)\vc{d}_1(S)\vc{D}_1 
	+ \lambda_2(S,\Delta_1,\Delta_2)\vc{e}_z\vc{e}_z,
\end{align*}
by writing $\ts{F} = \Fe\Fva$ where $\Fe$ corresponds to the elastic deformation gradient and
$\Fva$ to \emph{anelastic deformations}. which will be prescribed independently
(although their time evolution may depend on the current stress),
see \cite{Jallon+Etienne.2024.1}.


\begin{hypothesis}[shear-free, spanwise invariant anelastic prestretch]
The anelastic deformation gradient $\Fva$ is of the form $\Fva= \gamma_0\vc{D}_0\vc{D}_0+\gamma_1\vc{D}_1\vc{D}_1+\gamma_2\vc{D}_2\vc{D}_2$,
where $\gamma_i(S)>0$.
\end{hypothesis}
Thus, there is an intermediate configuration of the rod following these deformations which is such that
$$
\hat{\vc{X}}(\vc{X}) = \hat{\vc{R}}_c(S)  + \gamma_1 \Delta_1 \vc{D}_1 + \gamma_2 \Delta_2 \vc{D}_2
$$
where 
$\hat{\vc{R}}_c = \hat{\vc{R}}_0 + \intd{0}{S}{ \gamma_0(S) }{S} \vc{D}_0$
and we can choose a start point $\hat{\vc{R}}_0=\vc{R}_0$ identical to the one of the primary configuration.
As a result, 
%is the only elastic stretch that depends on the spanwise coordinates, and we can write
\begin{align*}
\Fe =& \alpha_0(S,\Delta_1,\Delta_2) \vc{d}_0(S,\Delta_1,\Delta_2)\vc{D}_0 
        \\&
        + \alpha_1(S,\Delta_1,\Delta_2)\vc{d}_1(S)\vc{D}_1 
        + \alpha_2(S,\Delta_1,\Delta_2)\vc{e}_z\vc{e}_z,
\end{align*}
where $\alpha_i := \lambda_i \gamma_i^{-1}$, $i \in \{0,1,2\}$, and in particular 
$$
\alpha_0(S,\Delta_1,\Delta_2)  = (1 - \delta_1(\Delta_1,\Delta_2)\kappa(S))\lambda(S)/\gamma_0(S).
$$

\subsection{Compatible deformation}

Under the Kirchhoff rod assumptions, the 3D stress is assumed to be of the form
$\ts{\sigma} = \sigma_{00} \vc{d}_0\vc{d}_0$ \cite{Audoly-Pomeau.2010.1},\JE{or is it along $\vc{d}\vc{d}$ ??}.
The assumption of zero transverse stress, $\sigma_{ii}=0$ for $i\in\{1,2\}$, leads to
$\alpha_{i}^2 -1 = -\nu(\alpha_{0}^2 - 1)$ and hence $\lambda_i = \gamma_i \sqrt{1+\nu(1-\alpha_0^2)}$.

The current configuration is thus characterised by a system of ODEs in $\delta_i$:
\begin{align}
\ded{\delta_i}{\Delta_i} = \gamma_i \sqrt{1+\nu-\nu\alpha^2(1-\kappa\delta_1)^2}
\label{eq-deltai}
\end{align}
where we have introduced $\alpha={\lambda}/{\gamma_0}$ the \emph{elastic stretch of the centreline}.
\JE{plus additionnal compatibility condition $\ded{\delta_1}{\Delta_2} +\ded{\delta_2}{\Delta_1}=0$ : where does it come from ?
it's the shear, and we've taken it 0. But explicitly?
}
This ODE system does not have obvious analytical solution, but both equations can be expanded for small rod thickness, 
$$
\ded{\delta_i}{\Delta_i} = \gamma_i a(\lambda) + \frac{\kappa\nu\gamma_i}{a(\lambda)} \delta_1(\Delta_1,\Delta_2) + O\left(\frac{W}{L'}\right)^2
$$
with $a(\lambda)=\sqrt{1+\nu\left(1-\alpha^2\right)}$. We must additionally match the condition 
$$
\ded{\delta_1}{\Delta_2} + \ded{\delta_2}{\Delta_1} = 0.
$$
Inspired by \cite{Audoly-Pomeau.2010.1}, 
we use an ansatz to obtain:
\begin{subequations}
\label{eq-deltai-taylor}
\begin{align*}
\delta_1 &=  \gamma_1 a(\lambda) \Delta_1 + \frac{\kappa\nu\gamma_1}{2} (\gamma_1\Delta_1^2 - \gamma_2\Delta_2^2) +  O\left(\frac{W^3}{L'^2}\right),
\\
\delta_2 &=  \gamma_2 a(\lambda) \Delta_2 + {\kappa\nu\gamma_1\gamma_2} \Delta_1 \Delta_2 + O\left(\frac{W^3}{L'^2}\right),
\end{align*}
\end{subequations}
which happens to fulfill all conditions, thanks to compatible choices in the modelling above.

The Jacobian of the transformation between the primary and deformed configurations of a section $\mathcal{S}(S)$
can be calculated up to order 3 by combining \eqref{eq-deltai}
for the diagonal terms and \eqref{eq-deltai-taylor} for the off-diagonal ones:
\begin{align*}
J =& \ded{\delta_1}{\Delta_1}\ded{\delta_2}{\Delta_2} - \ded{\delta_1}{\Delta_2}\ded{\delta_2}{\Delta_1}
	\\
	=& \gamma_1\gamma_2(1+\nu-\nu\alpha^2(1-2\kappa\delta_1+(\kappa\delta_1)^2)) + (\gamma_1\gamma_2\kappa\nu\Delta_2)^2 + O(W/L')^3
	\\
	=& \gamma_1\gamma_2\left(a^2(\lambda)+2\nu\kappa\alpha^2\left(\gamma_1 a(\lambda) \Delta_1 + \frac{\kappa\nu\gamma_1}{2} (\gamma_1\Delta_1^2 - \gamma_2\Delta_2^2)\right)
		-\nu\kappa^2\gamma_1^2 a^2(\lambda)\alpha^2 \Delta_1^2\right) 
		\\
		&+ (\gamma_1\gamma_2\kappa\nu\Delta_2)^2 + O(W/L')^3
		\\
	=& \gamma_1\gamma_2\left( a^2(\lambda) + 2\nu\gamma_1\kappa a(\lambda) \alpha^2 \Delta_1
		+ \nu\gamma_1^2\kappa^2\alpha^2 (\nu - a^2(\lambda) )\Delta_1^2 + \gamma_1\gamma_2 (\kappa\nu)^2 (1 - \alpha^2) \Delta_2^2 \right) + O(W/L')^3
\end{align*}
\JE{Ouch. Expression not checked, it seems weird...}

\paragraph{Limit of small deformations.} 
If ${\alpha} - 1 \ll 1$, then $a(\lambda) = 1 - \nu({\alpha}-1) + o({\alpha})$, and:
\begin{align*}
\delta_1 &=  \gamma_1 \Delta_1 + \frac{\nu\gamma_1}{2} \left(\kappa\gamma_1\Delta_1^2 - \kappa\gamma_2\Delta_2^2 - 2\left(\alpha - 1\right)\Delta_1\right) 
	+  ...
\\
\delta_2 &=  \gamma_2 \Delta_2 + {\nu\gamma_1\gamma_2} \left( \kappa \Delta_1 - \left(\alpha - 1\right)\right) \Delta_2 
	+ ...
\end{align*}
which is consistent with \cite[eq 3.17]{Audoly-Pomeau.2010.1}.
\hide{
\JE[If $\alpha_0-1 =o(1)$, then 
$$
\frac{\delta_2}{\Delta_2} = \gamma_2 (1 + \nu (1-\alpha_0))  + o(\alpha_0-1)
  = \gamma_2 - \frac{\nu\gamma_2 (\lambda-1)}{\gamma_0} + \frac{\nu\gamma_2\kappa\delta_1}{\gamma_0} + o(\alpha_0-1)
$$]{suppresed}}

\subsection{Constitutive relation, internal force and moment}

%We now impose the constitutive relation in \cite{Jallon+Etienne.2024.1} \JE[to which we add an isotropic part
%controled by Poisson's ratio]{?}:
Choosing an \JE[Ogden neo-Hookean strain energy density]{?}, 
$$
\rho\phi = \frac{G}{2}\left(\tr(\Fe\Fe\trsp-\ts{I}) - 2 \ln \det(\Fe\Fe\trsp)\right) + \frac{G\nu}{1-2\nu}\left(\det(\Fe\Fe\trsp)-1\right)^2,
$$
we have the Cauhy stress
$$
\ts{\sigma} = G(\Fe\Fe\trsp - \ts{I}) +\frac{G\nu}{1-2\nu}\tr(\Fe\Fe\trsp - \ts{I})\ts{I}
$$
The constitutive relation can be inverted by substracting to it its own trace multiplied by $\fracl{\nu}{1+\nu}\ts{I}$,
yielding:
$$
G(\Fe\Fe\trsp - \ts{I}) = \tsigma - \frac{\nu}{1+\nu}(\tr\tsigma)\ts{I}.
$$

We get
$\sigma_{00} = E(\alpha_0^2-1)/2 = E((1 - \delta_1\kappa)^2 \alpha^2 - 1)/2$
where we have introduced Young's modulus $E = 2G(1+\nu)$.

\JE{First strategy - need to inject J up to higher order it seems}
Since $\ts{\sigma} = \sigma_{00} \vc{d}_0\vc{d}_0$, the lineic density of elastic energy is
\begin{align*}
\dd{\mathcal{E}}{S} =& \frac{1}{2} \iintd{\vc{\Phi}(\mathcal{S})}{}{ \sigma_{00} (\alpha_0^2 - 1) }{\delta_1}{\delta_2}
	\\
	=& \frac{E}{4}  \iintd{\mathcal{S}}{}{ ((1 - \delta_1(\Delta_1)\kappa)^2 \alpha^2 - 1)^2 J}{\Delta_1}{\Delta_2} 
	\\
	=& \frac{E}{4} {\gamma_1\gamma_2}{} a(\lambda)^2 \iintd{\mathcal{S}}{}{ ((1 - \delta_1\kappa)^2 \alpha^2 - 1)^2 }{\Delta_1}{\Delta_2} 
	\\
	&+ \kappa \frac{E\nu}{2} \gamma_1^2\gamma_2 a(\lambda) \iintd{\mathcal{S}}{}{ ((1 - \delta_1\kappa)^2 \alpha^2 - 1)^2 \Delta_1}{\Delta_1}{\Delta_2} + O(EW^4/L'^2)
\end{align*}
\JE[By symmetry, odd terms in $\Delta_i$ vanish, so up to the second order we have:
$$
\dd{\mathcal{E}}{S} = \frac{E|\mathcal{S}|{\gamma_1\gamma_2}}{4}  \left(1 - \nu\left(\alpha^2 - 1\right) \right)\left(\alpha^2 - 1\right)^2
$$
]{and so $J$ would need to be one more order !! Need to resort to original $\dedl{\delta_i}{\Delta_i}$....}

\JE{Alternative strategy}
Since $\ts{\sigma} = \sigma_{00} \vc{d}_0\vc{d}_0$, the lineic density of elastic energy is
\begin{align*}
\dd{\mathcal{E}}{S} =& \frac{1}{2} \iintd{\vc{\Phi}(\mathcal{S})}{}{ \sigma_{00} (\alpha_0^2 - 1) }{\delta_1}{\delta_2}
	\\
	=& \frac{E}{4}  \iintd{\vc{\Phi}(\mathcal{S})}{}{ ((1 - \delta_1\kappa)^2 \alpha^2 - 1)^2 }{\delta_1}{\delta_2} 
	\\
	=& \frac{E}{4} |\vc{\Phi}(\mathcal{S})| (1-\alpha^2)^2 
		+ E\kappa (1-\alpha^2)\alpha^2 \iintd{\vc{\Phi}(\mathcal{S})}{}{ \delta_1 }{\delta_1}{\delta_2} 
		+ \frac{E}{2}\kappa^2 (3\alpha^2-1) \alpha^2 \iintd{\vc{\Phi}(\mathcal{S})}{}{ \delta_1^2 }{\delta_1}{\delta_2} 
		+ O(EW^5/L'^3)
\end{align*}

%\subsection{Bending energy and moment}

\hide{
\section{Spontaneous curvature}
\JE{NOT DONE}

We build on the approach of \cite{Audoly-Pomeau.2010.1} for Kirchhoff rods, but use the
strain energy derived in \cite{Jallon+Etienne.2024.1}.

We consider what will be called the \emph{primary configuration} of a slender elastic body.
Its \emph{centreline} is contained in the plane $z=0$,
$\vc{R}_c(S) = X_c({S})\vc{e}_x + Y_c({S})\vc{e}_y$,
for ${S} \in [0,{L}]$. Material points position can be described using a local frame 
with $\vc{D}_0 = \dedl{\vc{R}_c}{S}$, $\vc{D}_2 = \vc{e}_z$ and $\vc{D}_1 = - \vc{D}_0 \times \vc{D}_2$,
so that:
$$
{\pt{X}}({S},\Delta_1,\Delta_2) = {\pt{R}}_c({S}) + \Delta_1 {\vc{D}}_1({S}) + \Delta_2 \vc{e}_z
$$
for ${S} \in [0,{L}]$, $\Delta_1,\Delta_2 \in \mathcal{S}({S})$, with $\mathcal{S} \in \mathcal{B}_2(W)$ the
ball of $\Reels^2$ of radius $W \ll L$.
\JE{Can $\mathcal{S}({S})$?}

Let $\ts{F}$ be the deformation tensor.
The unshearable rods hypothesis states:
\JE{Is that it?}
\begin{hypothesis}[Euler--Bernoulli]
There exists a local frame $(\vc{d}_0,\vc{d}_1,\vc{d}_2)(S)$ such that the restriction of
the deformation tensor on the centreline can be written:
$$\ts{F}(\pt{R}_c(S)) = \lambda_0\vc{d}_0\vc{D}_0 + \lambda_1\vc{d}_1\vc{D}_1 + \lambda_2\vc{d}_2\vc{D}_2.$$
where $\lambda_i(S)$ is the stretch along direction $i$.
\end{hypothesis}
We will sometimes shorten $\lambda=\lambda_0$, the tangential stretch.

Let us verify the consistency of this hypothesis with the ansatz that the current
configuration of point $\pt{X}(S,\Delta_1,\Delta_2)$ is:
$$
\pt{x}(S,\Delta_1,\Delta_2) = \pt{r}_c(S) + \delta_1 \vc{d}_1(S) + \delta_2 \vc{d}_2.
$$
We then have $\vc{d}_0 = \partial_S \pt{r}_c = ...$

Let $\vc{r}(S;\Delta_1,\Delta_2)$ be the current configuration of a rod, $\max_i \Delta_i \leq W \ll \min \{\kappa^{-1},L\}$
where $\kappa$ is the curvature of the curve $\vc{r}(S;0,0)$ and $L$ a typical length. 
We consider only cases where there is no twist, $\ddt{...} = 0$
Without loss of generality, we consider the region $S \ll L$ and choose the frame such that ...
\JE{curve in plane $X,Z$ locally}

\JE{Here I directly take their result for the centreline, is it ok? They had some weird $X_0$ with unspecified $S$ dependence.}
From \cite{Audoly-Pomeau.2010.1}, this implies that 
\begin{align*}
\iintd{\mathcal{S}}{}{ \Delta_i }{\Delta_1}{\Delta_2} =& 0
&
i \in \{1,2\}
\end{align*}
\JE{and under assumptions }

Introduce a local basis $(\vc{e}_0,\vc{e}_1,\vc{e}_2))=(\vc{d}_0,\vc{d}_1,\vc{d}_2)(s=s_0)$.


the shear writes:
}
