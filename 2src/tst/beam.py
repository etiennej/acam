#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

"""
Test case: horizontal clamped rod in a gravity field
---
Will validate:
* non-regression of beam model
* non-regression of analytic Jacobian
"""

import sys,os
# allow to find classes in the parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from class_cortex import *
from common_utils import *
import time
import datetime
try:
    import cpuinfo
    mycpuinfo = cpuinfo.get_cpu_info()['brand_raw']
except:
    mycpuinfo = "Unknown CPU -- install py-cpuinfo to record it"


# Tolerance of non-regression
tol=1e-5
# Set to False to run test.
# Set to True to regenerate non-regression test results. /!\
build_test=False
if build_test:
    yes = input("Confirm recalculation of nonregression test results. yes/[no]:")
    if yes!='yes':
        print("Running as a test, remember to turn build_test=False in the code")
        build_test=False

 

# Compute the equilibrium shape of a beam with clamped end in gravity field
kappa_list=[1,1e-1,1e-3,1e-5]
gravity_list=[.01,.1]
# Frictional damping is used and reduced to 0. 

def gravity_field(grid,x,z):
    global omega
    return [np.zeros_like(x), -omega*np.ones_like(z)]

logger=Logger(logger_quiet_mode())

if build_test:
    cases = []
    for kappa in kappa_list:
     for gravity in gravity_list:
      for analytical_Jacobian in [True,False]:
            cases += [ { 'kappa':kappa, 'gravity':gravity, 'analytical_Jacobian':analytical_Jacobian,
                         'cpu': mycpuinfo, 'date':str(datetime.datetime.today()) } ]
else:
    cases = np.load("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], allow_pickle=True)

perf=[]
for case in cases:
    print(case)
    kappa = case['kappa']
    omega = case['gravity']
    cortex_par=cortex_par_defaults()
    cortex_par["logger"]=logger
    cortex_par["verbose"]=False
    cortex_par["Uzawa_verbosity"]=0
    cortex_par["boundary_conditions"]='custom'
    cx = Cortex({'shape':'segment', 'from':(0,0), 'to':(1,0), 'N':100}, cortex_par)
    cx.cortex_par['analytical_Jacobian'] = case['analytical_Jacobian']
    cx.add_external_force(gravity_field)
    cx.set_custom_separate_bc(cx.clamped_bc,cx.homogeneous_neumann_bc)

    t0=time.process_time()
    for damping in [*np.logspace(2, -4, 19),0]:
        cx.cortex_par["kappa"]=kappa
        cx.cortex_par["frictional_damping"]=damping
        res=cx.elastic_equilibrium()
        E_el = cx.elastic_energy()
        E_bg = cx.bending_energy()
        if res!=0: break
    success_status = res 
    if res==0:
        cputime=time.process_time()-t0
        deflection=cx.y()[-1]
        contourlength=np.trapz(cx.alpha(),cx.mesh.get_s_with_periodic_endpoint())
    if build_test:
        if res==0:
            case['deflection']=deflection
            case['contourlength']=contourlength
            case['cputime']=cputime
            case['success']=True
        else:
            case['success']=False
        print(case)
    else:
        fail=False
        if res>0 and case['success']:
            print ("Not converging when it used to.\n",case)
            exit(1)
        if case['success']:
            if abs(case['deflection']-deflection)/deflection > tol \
            or abs(case['contourlength']-contourlength)/contourlength > tol:
                print ("Not within tolerance.\n",case)
                exit(1)
            perf+=[ cputime/case['cputime'] ]


if build_test:
    np.save("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], cases)
else:
    print("Success with median relative performance ", np.median(perf))
    if np.median(perf)>1.1:
       print("Performance unexpectedly poor, marked as failed")
       exit(1)
