#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Authors : Alexander Nestor-Bergmann, Jocelyn Etienne
# (CC) october 2022 -- present
# =============================================================================

# data
import numpy as np
#import dill
from sklearn.neighbors import NearestNeighbors
#shapely is relatively slow as of 2023
#import shapely.geometry as geom

# plotting
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.colors as pltcolors
#from matplotlib.colors import ListedColormap, BoundaryNorm

# numerics
from scipy.interpolate import interp1d
from scipy.integrate import solve_bvp, cumulative_trapezoid 
from scipy.special import ellipeinc
from periodic_gradient import *

from typing import List, Tuple

from class_log import *
from class_mesh import *
from class_field import *

def cortex_plot_par_defaults():
    return { 'color':'k', 'colormap':'seismic', 'opacity':0.8, 'width':2,
        'equal_axes':True, 'plot_field':'alpha', 'plot_prestrain':True, 'label':None,
        'label_size':None, 'draw_colorbox':True }
def cortex_par_defaults():
    return { 'kappa':1e-1, 'boundary_conditions':'unset', 
        'inv_tau_c':1,
        'target_area':None,
        'elastic_tolerance':1e-3, 'max_nodes':30000 , 'analytical_Jacobian':False,
        'constraint_eps':1e-5,
        'Uzawa_tolerance':1e-3, 'Uzawa_rho':1, 'Uzawa_maxiter':1000, 'Uzawa_verbosity': 1,
        'verbose':True, 'logger':Logger(logger_debug_mode()) }

def build_cortex(par : dict, logger):
    try:
        N=par['N'] 
        if par['shape']=='circle':
            #phi_with_periodic_endpoints=np.linspace(*par['phi_range'],N+par['periodic'])
            #phi=phi_with_periodic_endpoints[0:N] # no duplicate point at the ends if periodic
            R=par['radius']
            m=Mesh(N, periodic=par["periodic"], s_range=par['phi_range'])
            NN=m.N_total()
            f=Field(m, np.array([ 
                np.zeros(NN),
				np.ones(NN)/R,
				m.get_s_with_periodic_endpoint()+np.pi/2,
				np.ones(NN),
				R*np.cos(m.get_s_with_periodic_endpoint())+par['center'][0],
				R*np.sin(m.get_s_with_periodic_endpoint())+par['center'][1] ]),
                #with_duplicate_endpoints=True,
                fieldnames=Cortex.psi_fieldnames)
            return [m, f]
                    #np.array(R*phi_with_periodic_endpoints), np.array(R*phi),
                    #np.array([ np.zeros(N), np.ones(N)/R, phi+np.pi/2, np.ones(N), R*np.cos(phi)+par['center'][0], R*np.sin(phi)+par['center'][1] ]) ]
        elif par['shape']=='ellipse':
            phi_with_periodic_endpoints=np.linspace(*par['phi_range'],N+par['periodic'])
            phi=phi_with_periodic_endpoints[0:N] # no duplicate point at the ends if periodic
            if 'angle' in par.keys():
                alpha=par['angle']
            else: alpha=0
            R=par['radii']
            e2=1-(R[1]/R[0])**2
            x=R[0]*np.cos(phi+alpha)+par['center'][0]
            y=R[1]*np.sin(phi+alpha)+par['center'][1]
            s_with_periodic_endpoints=ellipeinc(phi_with_periodic_endpoints,e2)
            c=1/(R[0]*R[1])**2*(np.cos(phi+alpha)**2/R[0]**2 + np.sin(phi+alpha)**2/R[1]**2)**(-3/2.)
            theta=np.arctan2(R[1]*np.cos(phi),-R[0]*np.sin(phi))+alpha-2*np.pi
            # make theta an increasing function over the range
            theta=theta+2*np.pi*np.cumsum(theta-np.roll(theta,1)<0)
            # not calculating derivative of curvature, trust algorithm to get it
            m=Mesh(N, periodic=par["periodic"], s=s_with_periodic_endpoints)
            if par["periodic"]:
                return [m, Field(m, np.array([ np.zeros(N), c, theta, np.ones(N), x, y ]), 
                    periodicity_offset=Cortex.periodicity_offset, fieldnames=Cortex.psi_fieldnames) ]
                
            else:
                return [m, Field(m, np.array([ np.zeros(N), c, theta, np.ones(N), x, y ]), fieldnames=Cortex.psi_fieldnames) ]
        elif par['shape']=='segment':
            assert not par['periodic'], "Segment can't be periodic"
            A=np.array(par['from'])
            B=np.array(par['to'])
            L=np.linalg.norm(B-A)
            m=Mesh(N, periodic=False, s_range=(0,L))
            xy=np.tensordot(m.get_s(),(B-A),axes=0)+A
            theta=np.arctan2(B[1]-A[1],B[0]-A[0])*np.ones(N)
            f=Field(m, np.array([
                        np.zeros(N),
                        np.zeros(N),
                        theta,
                        np.ones(N),
                        xy[:,0],
                        xy[:,1]
                    ]),
                    fieldnames=Cortex.psi_fieldnames)
            return [m, f]
        else:
            logger.user(0, 'Error, unknown shape %s' % par['shape'])
            exit()
    except Exception as e: logger.algorithmics(0, 'Initial shape error\n',e); exit()

def get_diff_curvature(some_psi): return some_psi[0]
def get_curvature(some_psi): return some_psi[1]
def get_theta(some_psi): return some_psi[2]
def get_alpha(some_psi): return some_psi[3]
def get_x(some_psi): return some_psi[4]
def get_y(some_psi): return some_psi[5]
def get_xy(some_psi): return field(some_psi.mesh,np.array([some_psi[4:6].T.reshape(-1, 2)]))

class Cortex(object):
    """
    Class that represents the apical cell cortex and holds all variables and functions to manipulate it.

    Attributes
    ----------
    mesh : Mesh
        mesh[i] curvilinear coordinate of node i in the initial configuration
        based on the Mesh class
    active_stretch : array[float]
        active_stretch[i] active stretch at i, active_stretch = 1 + active_stress/shear_modulus
    gamma : array[float]
        gamma[i] relaxation at i 
    psi : array[array[float]]
        mechanical state of the cortex -- see Methods for accessors
        psi = [θ**, θ*, θ, α, x, y]
    cortex_par : dict
        parameters, including
            boundary_condition in ['periodic','custom']
                

    Methods
    -------
    __init__(self, initial_guess: dict, cortex_par : dict) -> None
        constructor
    derivatives_linear_elastic_2D(self, grid, psi : array[array[float]] ) -> psi* : array[array[float]]
        returns derivatives for ODE solver
    bc_periodic(self, psi_minus : array[float], psi_plus : array[float]) -> array[float]
        implements periodic BCs for ODE solver
    set_custom_bc(self, bc_function : (psi_minus : array[float], psi_plus : array[float]) -> array[float]) 
        provides an external function to define BCs
    x,y -> array[float]
        coordinates in current configuration
    xy -> array[array[float]]
        coordinates in current configuration, x=xy[:,0], y=xy[:,1]
    theta -> array[float]
        angle with Ox in current configuration
    thetastar -> array[float]
        derivative of theta wrt mesh
    """
    psi_fieldnames = ['diff_curvature','curvature','theta','alpha','x','y']
    periodicity_offset = np.array([0,0,2*np.pi,0,0,0])

    def __init__(self, initial : dict, cortex_par : dict = None):
        if cortex_par==None: 
            self.cortex_par=cortex_par_defaults()
        else: self.cortex_par = cortex_par
        if 'psi' in initial : 
            self.psi = initial['psi']
            self.mesh  = initial['mesh']
        elif 'shape' in initial:  
            if initial.get('phi_range') in [None]:
                if self.cortex_par.get('symmetry') in ['vertical']:
                    initial['phi_range']=(-np.pi/2,np.pi/2)
                else:
                    initial['phi_range']=(0,2*np.pi)
                    initial['periodic']=(self.cortex_par['boundary_conditions']=='periodic')
            self.mesh,self.psi = build_cortex(initial, cortex_par['logger'])
        else: self.cortex_par['logger'].user(0, 'Unknown initial guess', initial_guess); exit(1)
        if 'active_stretch' in initial:
            self.active_stretch = initial['active_stretch']
        elif 'active_stress' in initial:
            self.active_stretch = 1+initial['active_stress']/initial['shear_modulus']
        else:
            self.active_stretch = Field(self.mesh, value=1.)   #np.ones_like(self.mesh.get_s_with_periodic_endpoint())
        self.gamma = Field(self.mesh, value=1.) #np.ones_like(self.mesh.get_s_with_periodic_endpoint())
        self.external_forces_list=[]
        self.adhesion_list=[]
        self.external_forces_theta_list=[]
        self.Lagrange_multipliers={"name":[], "value":[], "force":[]}
        self.initialize_before_run()

    def initialize_before_run(self):
        """
        take into account any change in `cortex_par', reset distance function
        """
        self.verbose_bvp=np.min([np.max([0,self.cortex_par['logger'].par['numerics']-3]),2])
        self.is_at_mechanical_equilibrium=False
        self.has_distance_function=False
        #self.the_gradient = np.gradient
        #self.psi_with_periodic_endpoint=self.psi
        if self.cortex_par.get('boundary_conditions')=='unset':
            self.cortex_par['logger'].user(0,"Please set the boundary condition")
            exit(1)
        if self.cortex_par.get('frictional_damping') not in [None, '0'] \
            and self.frictional_damping not in self.external_forces_list:
                self.external_forces_list+=[self.frictional_damping]
        if self.cortex_par.get('target_area') not in [None, 'none']:
            self.add_Lagrange_multiplier("pressure",self.pressure_force)
            if self.cortex_par['target_area']=='initial': self.cortex_par['target_area']=self.enclosed_area()
            if self.cortex_par['logger'].par['check_numerics']>=1:
                if abs(self.cortex_par['target_area']/self.enclosed_area()-1)>.01:
                    self.cortex_par['logger'].numerics(1,"Warning, large error on the area.")
            self.elastic_equilibrium=self.elastic_equilibrium_constrained
            if ('area' not in self.psi.fieldnames): # add area component
                area_field = Field(self.mesh, fieldnames=['area'], periodicity_offset=self.cortex_par['target_area']) 
                self.psi=join_components(self.psi,area_field)
            self.periodicity_area_offset = np.concatenate([ self.periodicity_offset, [self.cortex_par['target_area'] ]])
            if self.cortex_par.get('boundary_conditions')=='periodic':
                if self.cortex_par.get('symmetry') in ['vertical']:
                    self.boundary_conditions = self.vertical_axis_symmetry_closed_incompressible_bc
                else:
                    self.boundary_conditions = self.periodic_incompressible_bc
                    # for periodic BCs without duplicated endpoint -- now handled by Field class
                    #self.psi_with_periodic_endpoint = np.append(self.psi,
                    #    np.transpose([self.psi[:,0]+self.periodicity_area_offset]),axis=1)
                    #self.the_gradient = periodic_gradient
            elif self.cortex_par.get('boundary_conditions')=='free':
                if self.cortex_par.get('symmetry') in ['vertical']:
                    self.cortex_par['logger'].algorithmics(0,"to be coded"); exit()
                else:
                    self.cortex_par['logger'].algorithmics(0,"to be coded"); exit()
        if len(self.Lagrange_multipliers["name"])==0:
            self.elastic_equilibrium=self.elastic_equilibrium_unconstrained
            if self.cortex_par.get('boundary_conditions')=='periodic':
                if self.cortex_par.get('symmetry') in ['vertical']:
                    self.boundary_conditions = self.vertical_axis_symmetry_closed_bc
                else:
                    self.boundary_conditions = self.periodic_bc
                    # to be coded for periodic BCs without duplicated endpoint -- now handled by Field class
                    #self.psi_with_periodic_endpoint = np.append(self.psi,
                    #    np.transpose([self.psi[:,0]+self.periodicity_offset]),axis=1)
                    #self.the_gradient = periodic_gradient
            elif self.cortex_par.get('boundary_conditions')=='free':
                if self.cortex_par.get('symmetry') in ['vertical']:
                    self.cortex_par['logger'].algorithmics(0,"to be coded"); exit(1)
                else:
                    self.set_custom_separate_bc(self.open_homogeneous_neumann_bc,self.open_homogeneous_neumann_bc)
        #all_gammas = ([self.gamma(), self.the_gradient(self.gamma(), self.mesh.get_s_with_periodic_endpoint())])
        #all_gammas = np.array(all_gammas + [ self.the_gradient(all_gammas[1], self.mesh.get_s_with_periodic_endpoint()) ])
        # Now using Mesh class to interpolate only when needed
        #self.interp_gamma = interp1d(self.mesh_with_periodic_endpoint, all_gammas)
        ## self.interp_psi = interp1d(self.mesh, self.psi)
        #self.interp_memorized_psi = interp1d(self.mesh_with_periodic_endpoint, self.psi_with_periodic_endpoint)
        self.memorized_psi = self.psi #interp1d(self.mesh_with_periodic_endpoint, self.psi_with_periodic_endpoint)

    def update_to_new_grid_after_convergence(self, sol):
        old_mesh = self.mesh
        old_psi = self.psi
        self.mesh = Mesh(s=sol.x,  periodic=(self.cortex_par.get('boundary_conditions')=='periodic'))
        # Seems we now transfer to the new mesh directly?
        #self.interp_gamma =  self.gamma.to_mesh(self.mesh)
        #self.interp_active_stretch =  self.active_stretch.to_mesh(self.mesh)
        self.gamma =  self.gamma.to_mesh(self.mesh)
        self.active_stretch =  self.active_stretch.to_mesh(self.mesh)
        if self.cortex_par.get('boundary_conditions')=='periodic':
            if self.cortex_par['logger'].par['check_numerics']>=1:
                periodic_error=np.linalg.norm((sol.y[:,-1]-sol.y[:,0])[:6]-self.periodicity_offset)
                if periodic_error>self.cortex_par['elastic_tolerance']:
                    self.cortex_par['logger'].numerics(1, "Warning: large error on periodicity, ", periodic_error, 
                        (sol.y[:,-1]-sol.y[:,0])[:6]-self.periodicity_offset)
            self.psi = Field(self.mesh,sol.y[:6,:-1],                  \
                    periodicity_offset=self.periodicity_offset,     \
                    fieldnames=self.psi_fieldnames)
        else:
            self.psi = Field(self.mesh,sol.y,                         \
                    periodicity_offset=self.periodicity_offset,     \
                    fieldnames=self.psi_fieldnames)
        if self.cortex_par.get('target_area') not in [None, 'none']:
            self.set_Lagrange_multipliers(sol.p)
        self.is_at_mechanical_equilibrium=True
        for adh in self.adhesion_list: adh.regrid_nearest(self,old_mesh,self.get_xy(old_psi))

    def elastic_energy(self):
        return 0.5*dot(self.alpha()-1,self.alpha()-1)

    def bending_energy(self):
        return .5*self.cortex_par['kappa']*dot(self.curvature(),self.curvature())

    def clamped_bc(self, psiend):
        """
        x = y = θ = 0
        """
        return [self.get_x(psiend), self.get_y(psiend), self.get_theta(psiend)]

    def homogeneous_neumann_bc(self, psiend):
        """
        cancelling boundary terms in Eqs S34,S36 of Nestor-Bergmann et al 2022:
        θ' =  θ'' = 0
        α = 1
        x,y position and angle θ of ends are free
        """
        return [self.get_alpha(psiend)-1, self.get_curvature(psiend), self.get_diff_curvature(psiend)]

    def periodic_bc(self, psiup, psidown):
        """
        conditions are periodic except that theta gains -2π
        """
        return psidown - psiup - self.periodicity_offset
    
    #def periodic_constrained_bc(self, psiup, psidown, p):
    #    """
    #    conditions are periodic 
    #        + 0 initialisation for constraint fields
    #        + final values of constraint fields (e.g, area for A)
    #    """
    #    return np.concatenate(self.periodic_bc(psiup, psidown),     \
    #                psiup[self.slice_of_constraint_fields],           \
    #                psidown[self.slice_of_constraint_fields] - self.bc_of_constraint_fields ]
    

    def periodic_incompressible_bc(self, psiup, psidown, p):
        """
        DELETE ONCE LAG MULT WORK
        conditions are periodic except that theta gains -2π, area gains the target area and is set to 0 for S_0=0.
        """
        return np.concatenate([psidown - psiup - self.periodicity_area_offset, [psiup[6]]])

    def vertical_axis_symmetry_closed_bc(self, psiup, psidown):
        """
        x=0 is a symmetry axis, cortex forms a closed loop with its mirror symmetry part: 
            θ_a=0, x_a=0, θ_a**=0
            θ_b=π, x_b=0, θ_b**=0
        """
        return [psiup[0], psidown[0], psiup[2], psidown[2]-np.pi, psiup[4], psidown[4]]

    def vertical_axis_symmetry_closed_incompressible_bc(self, psiup, psidown, p):
        """
        x=0 is a symmetry axis, cortex forms a closed loop with its mirror symmetry part: 
            θ_a=0, x_a=0, θ_a**=0
            θ_b=π, x_b=0, θ_b**=0
        incompressible:  
            area gains the target area and is set to 0 for S_0=0
        """
        return [psiup[0], psidown[0], psiup[2], psidown[2]-np.pi, psiup[4], psidown[4],
            psiup[6], psidown[6]-self.cortex_par['target_area']]

    def separate_bcs(self, psiup, psidown):
        return self.boundary_conditions_up(psiup) + self.boundary_conditions_down(psidown)

    def set_custom_separate_bc(self, bc_function_up, bc_function_down):
        """
        Each function "up" and "down" should return a list of 3 values that are 0 iff BC
        on their side is matched.
        """
        self.cortex_par['boundary_conditions']='custom'
        self.boundary_conditions = self.separate_bcs
        self.boundary_conditions_up = bc_function_up
        self.boundary_conditions_down = bc_function_down
    
    def set_custom_bc(self, bc_function):
        self.cortex_par['boundary_conditions']='custom'
        self.boundary_conditions = bc_function

    def test_custom_bc(self, psiup, psidown):
        try:
            self.cortex_par['logger'].physics(0,"Difference between prescribed BC and provided values:",self.boundary_conditions(psiup, psidown))
        except Exception as e: self.cortex_par['logger'].user(0,"Boundary condition error, have you set them?\n",e); exit(1)

    def get_diff_curvature(self, some_psi): return some_psi[0]
    def get_curvature(self, some_psi): return some_psi[1]
    def get_theta(self, some_psi): return some_psi[2]
    def get_alpha(self, some_psi): return some_psi[3]
    def get_x(self, some_psi): return some_psi[4]
    def get_y(self, some_psi): return some_psi[5]
    def get_xy(self, some_psi): return some_psi[4:6].T.reshape(-1, 2)

    def N(self): return(self.mesh.N)
    def curvature(self): return self.psi[1]
    def theta(self): return self.psi[2]
    def alpha(self): return self.psi[3]
    def x(self): return self.psi[4]
    def y(self): return self.psi[5]
    def xy(self): return self.psi[4:6].T.reshape(-1, 2)
    
    # Now using Mesh class to interpolate only when needed
    #def interp_theta(self,new_mesh): return self.interp_psi(new_mesh)[2]
    #def interp_alpha(self,new_mesh): return self.interp_psi(new_mesh)[3]
    #def interp_x(self,new_mesh): return self.interp_psi(new_mesh)[4]
    #def interp_y(self,new_mesh): return self.interp_psi(new_mesh)[5]
    #def interp_xy(self,new_mesh): return self.interp_psi(new_mesh)[4:6]

    #def interp_memorized_theta(self,new_mesh): return self.interp_memorized_psi(new_mesh)[2]
    #def interp_memorized_alpha(self,new_mesh): return self.interp_memorized_psi(new_mesh)[3]
    #def interp_memorized_x(self,new_mesh): return self.interp_memorized_psi(new_mesh)[4]
    #def interp_memorized_y(self,new_mesh): return self.interp_memorized_psi(new_mesh)[5]
    #def interp_memorized_xy(self,new_mesh): return self.interp_memorized_psi(new_mesh)[4:6]

    def add_external_force(self, fext, depends_on_theta=False):
        if (depends_on_theta):
            if fext not in self.external_forces_theta_list:
                self.external_forces_theta_list+=[ fext ]
            else:
                if self.cortex_par['logger'].par['check_algorithmics']: 
                    self.cortex_par['logger'].algorithmics(2, 'Not adding twice ', fext.__name__, ' to external forces')
        else:
            if fext not in self.external_forces_list:
                self.external_forces_list+=[ fext ]
            else:
                if self.cortex_par['logger'].par['check_algorithmics']: 
                    self.cortex_par['logger'].algorithmics(2, 'Not adding twice ', fext.__name__, ' to external forces')

    def add_adhesion(self, adh):
        self.adhesion_list+=[ adh ]

    def frictional_damping(self,grid,x,y):
        """ implements a restoring force """
        delta = self.memorized_psi(grid,"x:y") - np.array([x,y])
        self.friction_E = self.cortex_par['frictional_damping']*np.trapz(delta*delta,grid)
        self.friction_F = self.cortex_par['frictional_damping']*np.trapz(delta,grid)
        return self.cortex_par['frictional_damping']*delta

    def reaction(self,mesh,x,y):
        """ uses the reaction force stored in class, arising e.g. from constrained_elastic_equilibrium """
        return self.interp_reaction_force(mesh)

    def penalty_constraint_positive_y(self,mesh,x,y):
        """ adds a penalty for the contact problem """
        return self.cortex_par['Uzawa_rho']*(self.interp_gap_function(mesh) - y)

    def external_forces(self, grid,x,y,theta):
        f=[ np.array(fi(grid,x,y))       for fi in self.external_forces_list ]
        f+=[np.array(fi(grid,x,y,theta)) for fi in self.external_forces_theta_list ]
        f+=[adh.force(self, grid, [x,y]) for adh in self.adhesion_list]
        #for ffi,fi in zip(f,self.external_forces_list+self.external_forces_theta_list):
        #    print (fi, ffi.shape)
        if f==[]: return np.zeros_like(grid)
        else: 
            #print(type(f), [(fe,type(fi),fi.shape) for fe,fi in zip(np.concatenate([self.external_forces_list,self.external_forces_theta_list,self.adhesion_list]),f) ]); 
            return sum(f)

    def add_Lagrange_multiplier(self, name, force, default_value=0):
        if name not in self.Lagrange_multipliers["name"]:
            self.Lagrange_multipliers["name"]+=[name]
            self.Lagrange_multipliers["value"]+=[default_value]
            self.Lagrange_multipliers["force"]+=[force]
            #self.slice_of_constraint_fields=slice(len(self.psi_fieldnames),len(self.psi_fieldnames)+len(self.Lagrange_multipliers)
        else:
            self.cortex_par['logger'].algorithmics(2, 'Not adding twice ', name, ' to Lagrange multipliers')

    def set_Lagrange_multipliers(self, values):
        assert type(values) is np.ndarray
        self.Lagrange_multipliers["values"]=values

    def Lagrange_multiplier_force(self, mul, normal):
        #print(mul,type(mul))
        #if type(mul)==np.float64: # there must be one only
        #    return self.Lagrange_multipliers["function"][0](mul,normal)
        #else:
        return np.sum([ funi(muli, normal) 
            for muli,funi in zip(mul,self.Lagrange_multipliers["force"])], axis=0)

    def pressure_force(self, p, normal): # THERE SHOULD BE STRETCH??
        return p*normal
    def area_constraint(self, grid, psi : np.array, lagrange_mul: np.array): 
        D,C,theta,alpha,x,y,A = psi 
        # dA_ds = -(x,y)·n
        dA_dS0 = (x*np.sin(theta) - y*np.cos(theta))*alpha*self.gamma_cur[0]/2
        return [ dA_dS0 ]
    def area_periodicity(self):
        return 

    def pinx_force(self, mu_pinx, normal):
        return mu_pinx*np.array([np.ones_like(normal[0]), np.zeros_like(normal[1])])
    def piny_force(self, mu_piny, normal):
        return mu_piny*np.array([np.zeros_like(normal[0]), np.ones_like(normal[1])])

    def calculate_enclosed_area(self,x,y):
        """ calculate area using shoelace formula """
        return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))
    
    def enclosed_area(self):
        return self.calculate_enclosed_area(self.x(),self.y())

    def derivatives_linear_elastic_2D_constitutive_2025(self, grid, psi_on_grid : np.array, Lagrange_mult=None): 
        """
        Constitutive equation of 2025 J Elast paper, n_3 = E(alpha^2-1)
        """
        D,C,theta,alpha,x,y = psi_on_grid 
        kappa = self.cortex_par['kappa']
        # gamma and its derivatives evaluated on the grid
        self.gamma_cur = [self.gamma(grid), self.gamma.diff(grid), self.gamma.ddiff(grid)]
        tangent = np.array([np.cos(theta), np.sin(theta)])
        normal  = np.array([-tangent[1],   tangent[0]])
        #if (len(self.mesh)!=len(mesh_new) and (len(self.mesh)!=len(mesh_new)+1)) or (len(self.mesh)==len(mesh_new) and any(self.mesh!=mesh_new)):
        #    print("Mesh was adapted: ",len(set(self.mesh)&set(mesh_new)),len(self.mesh),len(mesh_new))
        # if len diff is 1, it's the staggered grid.
        fext = self.external_forces(grid,x,y,theta)
        # scalar Lagrange multiplier pressure term
        if Lagrange_mult is not None: fext += self.Lagrange_multiplier_force(Lagrange_mult, normal)
        dalpha_dS0 = kappa * C*(C*self.gamma_cur[1]/self.gamma_cur[0]-D)/alpha**2/self.gamma_cur[0]**2 /2    \
                    -self.gamma_cur[0] * (fext * tangent).sum(axis=0)/alpha /2
        dD_dS0 = D*(3*self.gamma_cur[1]/self.gamma_cur[0] + dalpha_dS0/alpha)/alpha                            \
                +C*( (self.gamma_cur[2]/self.gamma_cur[0] -3*(self.gamma_cur[1]/self.gamma_cur[0])**2        \
                    -self.gamma_cur[1]/self.gamma_cur[0]*dalpha_dS0/alpha)/alpha                                \
                    +1/kappa*self.gamma_cur[0]**2*(alpha**2-1) )                               \
                +1/kappa*self.gamma_cur[0]**3 * ( fext * normal).sum(axis=0)
        f = ([ dD_dS0, D, C, dalpha_dS0, np.cos(theta)*alpha*self.gamma_cur[0], np.sin(theta)*alpha*self.gamma_cur[0] ])
        return np.array([ dD_dS0, D, C, dalpha_dS0, np.cos(theta)*alpha*self.gamma_cur[0], np.sin(theta)*alpha*self.gamma_cur[0] ])

    def derivatives_linear_elastic_2D_constitutive_2022(self, grid, psi_on_grid : np.array, Lagrange_mult=None): 
        """
        Constitutive equation of 2022 PLoS paper, n_3 = E(alpha-1)
        """
        D,C,theta,alpha,x,y = psi_on_grid 
        kappa = self.cortex_par['kappa']
        # gamma and its derivatives evaluated on the grid
        self.gamma_cur = [self.gamma(grid), self.gamma.diff(grid), self.gamma.ddiff(grid)]
        tangent = np.array([np.cos(theta), np.sin(theta)])
        normal  = np.array([-tangent[1],   tangent[0]])
        #if (len(self.mesh)!=len(mesh_new) and (len(self.mesh)!=len(mesh_new)+1)) or (len(self.mesh)==len(mesh_new) and any(self.mesh!=mesh_new)):
        #    print("Mesh was adapted: ",len(set(self.mesh)&set(mesh_new)),len(self.mesh),len(mesh_new))
        # if len diff is 1, it's the staggered grid.
        fext = self.external_forces(grid,x,y,theta)
        # scalar Lagrange multiplier pressure term
        if Lagrange_mult is not None: fext += self.Lagrange_multiplier_force(Lagrange_mult, normal)
        dalpha_dS0 = kappa * C*(C*self.gamma_cur[1]/self.gamma_cur[0]-D)/alpha/self.gamma_cur[0]**2    \
                    -self.gamma_cur[0] * (fext * tangent).sum(axis=0)
        dD_dS0 = D*(3*self.gamma_cur[1]/self.gamma_cur[0] + dalpha_dS0/alpha)                            \
                +C*(self.gamma_cur[2]/self.gamma_cur[0] -3*(self.gamma_cur[1]/self.gamma_cur[0])**2        \
                    -self.gamma_cur[1]/self.gamma_cur[0]*dalpha_dS0/alpha                                \
                    +1/kappa*self.gamma_cur[0]**2*alpha*(alpha-1) )                               \
                +1/kappa*self.gamma_cur[0]**3*alpha * ( fext * normal).sum(axis=0)
        f = ([ dD_dS0, D, C, dalpha_dS0, np.cos(theta)*alpha*self.gamma_cur[0], np.sin(theta)*alpha*self.gamma_cur[0] ])
        return np.array([ dD_dS0, D, C, dalpha_dS0, np.cos(theta)*alpha*self.gamma_cur[0], np.sin(theta)*alpha*self.gamma_cur[0] ])
    
    derivatives_linear_elastic_2D = derivatives_linear_elastic_2D_constitutive_2025

    def jacobian_derivatives_linear_elastic_2D(self, mesh_new, psi: np.array, p=None):
        theta_2,theta_1,theta,alpha,x,y = psi[0:6]
        friction=0
        kappa = self.cortex_par['kappa']
        self.gamma_cur = self.interp_gamma(mesh_new)
        tangent = [np.cos(theta), np.sin(theta)]
        normal  = [-tangent[1],   tangent[0]] 
        fext = self.external_forces(mesh_new,x,y,theta)
        f_tan = (fext*tangent).sum(axis=0) # f_x(x, y)*cos(theta) + f_y(x, y)*sin(theta)
        f_norm = (fext*normal).sum(axis=0) # f_x(x, y)*sin(theta) - f_y(x, y)*cos(theta)
        if (p!=None): f_norm += p
        if self.cortex_par.get('frictional_damping') not in [None, '0']: friction=self.cortex_par['frictional_damping']
        #dfx_dx, dfy_dy = -friction*np.ones_like(psi[0:2]) 
        #dfy_dx, dfx_dy = np.zeros_like(psi[0:2])
        dfx_dx, dfx_dy, dfy_dx, dfx_dx = self.grad_external_forces(mesh_new,x,y,theta)
        alpha_gamma=alpha*self.gamma_cur[0]
        alpha_gamma2=alpha_gamma*self.gamma_cur[0]
        alpha2_gamma=alpha_gamma2*alpha
        gamma_ov_alpha=self.gamma_cur[0]/alpha
        gamma_1=self.gamma_cur[1]
        gamma_2=self.gamma_cur[2]
        gammastar_ov_alpha=gamma_1/alpha
        alpha_gamma3=alpha_gamma2*self.gamma_cur[0]
        ones=np.ones(len(psi[0]))
        zeros=np.zeros(len(psi[0]))
        J=np.zeros((6,6,len(psi[0])))
        J[:,:,:]=[[-gamma_ov_alpha*f_tan + 3*gamma_1/self.gamma_cur[0] 
                    + 2*kappa*(gamma_1*theta_1**2/(alpha_gamma*alpha_gamma2) - theta_1*theta_2/alpha_gamma**2),           
                alpha_gamma**2/kappa - alpha_gamma2/kappa + gammastar_ov_alpha*f_tan 
                    + gamma_2/self.gamma_cur[0] - 3*gamma_1**2/self.gamma_cur[0]**2 - 3*gamma_1**2*kappa*theta_1**2/alpha_gamma2**2 + 4*gamma_1*kappa*theta_1*theta_2/(alpha_gamma*alpha_gamma2) 
                    - kappa*theta_2**2/alpha_gamma**2, 
                -alpha_gamma3*f_tan/kappa - gamma_ov_alpha*theta_2*f_norm + gammastar_ov_alpha*theta_1*f_norm,
                2*alpha_gamma2*theta_1/kappa + self.gamma_cur[0]**3*f_norm/kappa - self.gamma_cur[0]**2*theta_1/kappa 
                    + 2*gamma_1**2*kappa*theta_1**3/(alpha_gamma**2*alpha_gamma2) - 4*gamma_1*kappa*theta_1**2*theta_2/alpha_gamma**3 
                    + 2*kappa*theta_1*theta_2**2/(alpha2_gamma*alpha_gamma) + gamma_ov_alpha*theta_2*f_tan/alpha 
                    - gammastar_ov_alpha*theta_1*f_tan/alpha, 
                -alpha_gamma3*dfx_dx*np.sin(theta)/kappa + alpha_gamma3*dfy_dx*np.cos(theta)/kappa 
                    - dfx_dx*gamma_ov_alpha*theta_2*np.cos(theta) + dfx_dx*gammastar_ov_alpha*theta_1*np.cos(theta) 
                    - dfy_dx*gamma_ov_alpha*theta_2*np.sin(theta) + dfy_dx*gammastar_ov_alpha*theta_1*np.sin(theta),
                -alpha_gamma3*dfx_dy*np.sin(theta)/kappa + alpha_gamma3*dfy_dy*np.cos(theta)/kappa 
                    - dfx_dy*gamma_ov_alpha*theta_2*np.cos(theta) + dfx_dy*gammastar_ov_alpha*theta_1*np.cos(theta) 
                    - dfy_dy*gamma_ov_alpha*theta_2*np.sin(theta) + dfy_dy*gammastar_ov_alpha*theta_1*np.sin(theta)], 
            [ ones, zeros, zeros, zeros, zeros, zeros], 
            [zeros,  ones, zeros, zeros, zeros, zeros], 
            [-kappa*theta_1/alpha_gamma2,
                2*gamma_1*kappa*theta_1/alpha_gamma3 - kappa*theta_2/alpha_gamma2,
                -self.gamma_cur[0]*f_norm,
                -gamma_1*kappa*theta_1**2/(alpha_gamma*alpha_gamma2) + kappa*theta_1*theta_2/alpha_gamma**2,
                -dfx_dx*self.gamma_cur[0]*np.cos(theta) - dfy_dx*self.gamma_cur[0]*np.sin(theta),
                -dfx_dy*self.gamma_cur[0]*np.cos(theta) - dfy_dy*self.gamma_cur[0]*np.sin(theta)],
            [zeros, zeros, -alpha_gamma*np.sin(theta), self.gamma_cur[0]*np.cos(theta), zeros, zeros],
            [zeros, zeros, alpha_gamma*np.cos(theta), self.gamma_cur[0]*np.sin(theta), zeros, zeros]]
        return J

    def test_jacobian(self,f,J,x,t,*args):
            n = len(x)
            N = len(x[0])
            s = [np.linalg.norm(xi)/N for xi in x]
            print(s, self.external_forces_list, self.external_forces_theta_list)
            z = np.ones(N)
            func = f(t,x,*args)
            numjac = np.zeros((n, n, N))
            for dx in np.logspace(-5,-9,5):
                for j in range(n):  
                  Dxj = (s[j]*dx if s[j] != 0 else dx)
                  x_plus = np.copy(x)
                  x_plus[j,:] += Dxj*z
                  #print(dx, Dxj, ( (x_plus - x)/Dxj))
                  #Dxj=1 ###############
                  numjac[:, j, :] = (f(t,x_plus,*args) - func)/Dxj
                  #if (j==5): print(" el 5: ",Dxj,s[j],func[0]-f(*args,x_plus)[0],numjac[0,5])
                #print((numjac).shape,(numjac-J(*args,x)).shape)
                theJ=J(t,x,*args)
                if len(args)>0: theJ=theJ[0]
                err=abs((numjac-theJ)[:,:,0:4])
                #err=abs((numjac-theJ))
                ie=list(np.unravel_index(np.argmax(err),err.shape))
                #print(dx," Errmin",np.min(err), " in ",np.unravel_index(np.argmin(err),err.shape))
                print(dx," Errmax",np.max(err), " in ",np.unravel_index(np.argmax(err),err.shape))
                ie[2]=slice(10)
                np.set_printoptions(precision=2)
                print("    ",theJ[ie],numjac[ie])
                print("  J=",theJ[:,:,0])
                print(" Jh=",numjac[:,:,0])

    def derivatives_linear_elastic_2D_constrained(self, grid, psi : np.array, lagrange_mul: np.array): 
        dpsi_dS0 = self.derivatives_linear_elastic_2D(grid, psi[0:6], lagrange_mul)
        D,C,theta,alpha,x,y,A = psi 
        dA_dS0 = (x*np.sin(theta)*alpha*self.gamma_cur[0] - y*np.cos(theta)*alpha*self.gamma_cur[0])/2
        return np.concatenate([ dpsi_dS0, [ dA_dS0 ] ])

    def jacobian_derivatives_linear_elastic_2D_area(self, grid, psi: np.array, lagrange_mul: np.array):
        Jpsi = self.jacobian_derivatives_linear_elastic_2D(mesh_new, psi[0:6], lagrange_mul[0])
        D,C,theta,alpha,x,y,A = psi
        z = np.zeros_like(psi[0])
        d2A_dS0_dpsi = np.array([[ z, z, 
            (x*np.cos(theta)*alpha*self.gamma_cur[0] + y*np.sin(theta)*alpha*self.gamma_cur[0])/2,
            (x*np.sin(theta)*self.gamma_cur[0] - y*np.cos(theta)*self.gamma_cur[0])/2,
            np.sin(theta)*alpha*self.gamma_cur[0]/2,
            - np.cos(theta)*alpha*self.gamma_cur[0]/2 
            ]])
        J = np.concatenate([ Jpsi, d2A_dS0_dpsi ])
        kappa = self.cortex_par['kappa']
        dFdp = np.concatenate([ [[alpha*self.gamma_cur[0]**3/kappa]], np.array([[z] for i in range(6)]) ])
        return np.concatenate([ J, np.array([[z] for i in range(7)])],axis=1), dFdp

    def elastic_equilibrium_unconstrained(self, no_save=False):
        """
        Calculate the elastic equilibrium of the cortex

        no_save : boolean
            If true, don't copy result into self.memorized_psi even in case of convergence.
            Use when calling from an iterative pseudo-time relaxation loop.
        """
        self.initialize_before_run()
        psi_array = self.psi.nodval()
        # Solve the BVP using Scipy
        sol = solve_bvp(self.derivatives_linear_elastic_2D, self.boundary_conditions,
                        self.mesh.get_s_with_periodic_endpoint(), psi_array, max_nodes=self.cortex_par['max_nodes'],
                        verbose=self.verbose_bvp, tol=self.cortex_par['elastic_tolerance'])
        # Check if the solver passed
        if sol.status != 0:
            self.cortex_par['logger'].numerics(0, "Warning: no convergence of elastic problem (%s)" % sol.message)
            return sol.status
        if not no_save: self.memorized_psi=self.psi
        self.update_to_new_grid_after_convergence(sol)
        return sol.status

    def elastic_equilibrium_fixed_area(self, no_save=False):
        """
        Calculate the elastic equilibrium of the cortex, with area constraint on

        no_save : boolean
            If true, don't copy result into self.memorized_psi even in case of convergence.
            Use when calling from an iterative pseudo-time relaxation loop.
        """
        self.initialize_before_run()
        print(self.memorized_psi.fieldnames)
        self.pressure=0
        psi_array = self.psi.nodval()
        # Solve the BVP using Scipy
        if self.cortex_par['analytical_Jacobian']:
            jac_fun={'fun_jac':self.jacobian_derivatives_linear_elastic_2D_area }
        else: jac_fun={}
        sol = solve_bvp(self.derivatives_linear_elastic_2D_area,  self.boundary_conditions,
                        self.mesh.get_s_with_periodic_endpoint(), psi_array,
                        p=np.array([self.pressure]), **jac_fun,
                        max_nodes=self.cortex_par['max_nodes'],
                        verbose=self.cortex_par['verbose'], tol=self.cortex_par['elastic_tolerance'])

        ## Test Jacobian
        # self.test_jacobian(self.derivatives_linear_elastic_2D_area, self.jacobian_derivatives_linear_elastic_2D_area, self.psi, self.mesh, np.array([self.pressure]))

        # Check if the solver passed
        if sol.status != 0:
            self.cortex_par['logger'].numerics(0, "Warning: no convergence of elastic problem (%s)" % sol.message)
            if "Jacobian" in sol.message and self.cortex_par['analytical_Jacobian']: 
                self.cortex_par['logger'].numerics(0, "Try cortex_par['analytical_Jacobian']=False.")
            return sol.status
        if not no_save: self.memorized_psi=self.psi
        self.update_to_new_grid_after_convergence(sol)
        return sol.status


    def elastic_equilibrium_constrained(self, no_save=False):
        """
        Calculate the elastic equilibrium of the cortex, with area and/or centering constraint on

        no_save : boolean
            If true, don't copy result into self.memorized_psi even in case of convergence.
            Use when calling from an iterative pseudo-time relaxation loop.
        """
        self.initialize_before_run()
        print(self.memorized_psi.fieldnames)
        psi_array = self.psi.nodval()
        Lagrange_multipliers_array = self.Lagrange_multipliers["value"]
        print(self.Lagrange_multipliers, Lagrange_multipliers_array)
        # Solve the BVP using Scipy
        sol = solve_bvp(self.derivatives_linear_elastic_2D_constrained,  self.boundary_conditions,
                        self.mesh.get_s_with_periodic_endpoint(), psi_array,
                        p=Lagrange_multipliers_array,
                        max_nodes=self.cortex_par['max_nodes'],
                        verbose=self.cortex_par['verbose'], tol=self.cortex_par['elastic_tolerance'])

        ## Test Jacobian
        # self.test_jacobian(self.derivatives_linear_elastic_2D_area, self.jacobian_derivatives_linear_elastic_2D_area, self.psi, self.mesh, np.array([self.pressure]))

        # Check if the solver passed
        if sol.status != 0:
            self.cortex_par['logger'].numerics(0, "Warning: no convergence of elastic problem (%s)" % sol.message)
            if "Jacobian" in sol.message and self.cortex_par['analytical_Jacobian']: 
                self.cortex_par['logger'].numerics(0, "Try cortex_par['analytical_Jacobian']=False.")
            return sol.status
        if not no_save: self.memorized_psi=self.psi
        self.update_to_new_grid_after_convergence(sol)
        return sol.status

    def set_constraint():
        return ERROR

    def constraint(self):
        return -self.y()*(self.y()<0)

    def constraint_admissible_part(self, f,constraint):
        return f*(self.y()<=self.cortex_par['constraint_eps'])

    def calculate_reaction_force(self, multiplier):
        #return np.transpose([multiplier])*np.array([0,1])
        return multiplier*np.array([[0],[1]])
  
    def constrained_elastic_equilibrium(self):
        self.initialize_before_run()
        multiplier=np.zeros_like(self.mesh)
        reaction_force=np.zeros((2,len(multiplier)))
        self.add_external_force(self.reaction)
        self.add_external_force(self.penalty_constraint_positive_y)
        residual=1
        # phi_c in Koko 2021
        # For tests, the substrate is the half-plane y<0
        gap_function=self.y()
        i=0
        status=0
        # Uzawa algorithm
        while residual>self.cortex_par['Uzawa_tolerance']:
            i+=1
            psi_prev=interp1d(self.mesh,self.psi)
            gap_function_prev=interp1d(self.mesh,gap_function)
            #print(self.mesh.shape,reaction_force.shape,self.psi.shape)
            self.interp_gap_function = interp1d(self.mesh, gap_function)
            self.interp_reaction_force_multiplier = interp1d(self.mesh,multiplier)
            self.interp_reaction_force = interp1d(self.mesh,reaction_force)
            status=self.elastic_equilibrium(no_save=False)

            if status!=0: break
            # note mesh will have changed
            # compute auxilliary contact variable, which will be either y or 0 locally
            gap_function = self.y() + 1/self.cortex_par['Uzawa_rho']*( self.interp_reaction_force_multiplier(self.mesh) - np.fmax(0, self.interp_reaction_force_multiplier(self.mesh) - self.cortex_par['Uzawa_rho']*(self.y())) )
            multiplier=self.cortex_par['Uzawa_rho']*(self.y() - gap_function) + self.interp_reaction_force_multiplier(self.mesh)
            # due to the fact that it's not a real operator...
            reaction_force=self.calculate_reaction_force(multiplier)
            residual=(np.linalg.norm(gap_function-gap_function_prev(self.mesh))**2 + np.linalg.norm(self.psi-psi_prev(self.mesh))**2 )/(np.linalg.norm(gap_function)**2 + np.linalg.norm(self.psi)**2 )
            iymin=self.y().argmin()
            self.cortex_par['logger'].numerics(4, 
                "[%d] Uzawa res=%.3e ymin=%.3e at x=%3e" % (i, residual, self.y()[iymin], self.x()[iymin]), flush=( (i%500)==0 ))
            if i>=self.cortex_par['Uzawa_maxiter']:
                self.cortex_par['logger'].numerics(1, "[%d] Uzawa max iterations reached"%i)
                status=1
                break
        # Solve the BVP using Scipy
        # self.is_at_mechanical_equilibrium=True
        if status==0: self.cortex_par['logger'].numerics(2, "[%d] Uzawa res=%.3e" % (i, residual))
        return status

    def relax(self, timestep):
        """
        Relax extensional constraints by applying 1/gamma d(gamma)/dt = 1 - 1/alpha
        """
        if self.cortex_par['logger'].par['check_numerics']>=2:
            if timestep*self.cortex_par['inv_tau_c'] > .1:
                self.cortex_par['logger'].numerics(1,"Warning. Timestep %.2e too large compared to relaxation time %.2e" % (timestep,1/self.cortex_par['inv_tau_c']))
        print(self.alpha().mesh.N, self.active_stretch.mesh.N, self.mesh.N, (self.alpha()**(-2)).mesh.N)
        self.gamma *= 1 + timestep/2*self.cortex_par['inv_tau_c']*( 1 - self.alpha()**(-2)*self.active_stretch ) 
        #s = cumulative_trapezoid(self.alpha(), self.mesh_with_periodic_endpoint, initial=0)
        #self.mesh_with_periodic_endpoint = (self.cortex_par['inv_tau_c']*s + self.mesh_with_periodic_endpoint/timestep)/(1/timestep+self.cortex_par['inv_tau_c'])
        #if self.cortex_par.get('boundary_conditions')=='periodic':
        #    self.mesh = self.mesh_with_periodic_endpoint[:-1]
        #else:
        #    self.mesh = self.mesh_with_periodic_endpoint 
    
    def nearest_within_radius(self, xy, r): 
        """ 
        return the node(s) on the cortex within distance r to points in list xy

        input
        -----
        xy : list[ points ]
        r  : float
            
        output
        ------
        k : array[array[float]]
            point xy[i] is closer than r from point with index k(i,j) in cortex
        """
        if not self.has_distance_function:
            self.distance_function = NearestNeighbors(radius=r, #self.cortex_par.get('adhesion_search_radius'),
                                    algorithm='auto', n_jobs=-1)
            self.distance_function.fit(self.xy())
            self.has_distance_function=True
        return self.distance_function.radius_neighbors(xy, r)

    def vector_to_nearest_within_radius(self, xy, r, signed_distance=False):
        """
        for each xy[i], return the index, distance and vector pointing from point xy[i] to a node of the cortex closer than r. 
        If signed_distance==True, the distance is the signed distance (positive when xy is outside cortex)
        """
        d,neighbors = self.nearest_within_radius(xy,r)
        sxy=self.xy()
        v = np.empty(len(neighbors),dtype=object)
        for i in range(len(neighbors)):
            v[i] = sxy[neighbors[i]] - xy[i]
            if signed_distance:
                d[i] *= np.sign(-v[i,:]*np.sin(self.theta()[neighbors[i]])+v[i,:]*np.cos(self.theta()[neighbors[i]]))
        return neighbors,d,v

    def nearest_node(self, xy, n=1): #, allinrange=False):
        """ 
        return the (n) closest node(s) on the cortex to points in list xy

        input
        -----
        xy : list[ points ]
            
        output
        ------
        d : array[array[float]]
        k : array[array[float]]
            d(i,j) distance from point xy[i] to point with index k(i,j) in cortex
        """
        if not self.has_distance_function:
            self.distance_function = NearestNeighbors(radius=1.0, #self.cortex_par.get('adhesion_search_radius'),
                                    algorithm='auto', n_jobs=-1)
            self.distance_function.fit(self.xy())
            self.has_distance_function=True
        return self.distance_function.kneighbors(xy, n)

    def vector_to_nearest_node(self, xy, signed_distance=True):
        """
        for all i, return the vector pointing from point xy[i] to the closest node of the cortex. 
        Note that this is not in general the closest point on the curve.
        If signed_distance==True, also return the signed distance
        """
        d,neighbors = self.nearest(xy)
        # only one point, can strip it out of list
        d=d[:,0]
        neighbors=neighbors[:,0]
        v = self.xy()[neighbors] - xy
        if signed_distance:
            d *= np.sign(-v[:,0]*np.sin(self.theta()[neighbors])+v[:,1]*np.cos(self.theta()[neighbors]))
            return v,d
        else:
            return v

    def vector_to_nearest_point(self, xy, signed_distance=True):
        """
        for all i, return the vector pointing from point xy[i] to the closest point on cortex.
        If signed_distance==True, also return the signed distance
        """
        d,neighbor = self.nearest_node(xy,2)
        # if the 2 closest neighbours are not everywhere the 2 ends of one element, then resolution is insufficient
        interval=neighbor[:,1]-neighbor[:,0]
        if not np.isin(interval, [1, -1, self.psi.shape[1]-1, 1-self.psi.shape[1]]).all():
            #print(interval, neighbor[:,0],neighbor[:,1],self.xy()[neighbor[0,:]], self.xy().shape[0]-1)
            raise Exception("Error, unexpected nearest points suggest the cortex is not smooth enough for this discretisation")
            exit(1)
        orig=self.xy()[neighbor[:,0]]
        edge=self.xy()[neighbor[:,1]]-orig
        # projection of xy[i] onto the edge[i]
        d=d[:,0]
        v = orig - xy
        s_loc = np.sum(-v*edge,axis=1)/np.linalg.norm(edge,axis=1)**2
        #s_loc = [-vi*ei/(ei[0]**2+
        is_within_edge = (0<s_loc) * (s_loc<1)
        for i in range(len(s_loc)):
            if is_within_edge[i]:
                v[i] += s_loc[i]*edge[i]
                #print ("here sloc:",s_loc, self.xy()[neighbor[i,0]], self.xy()[neighbor[i,0]]+s_loc[i]*edge[i], v[i], np.dot(v[i],edge[i]))
                d = np.linalg.norm(v)
        if signed_distance:
            d *= np.sign(-v[:,0]*np.sin(self.theta()[neighbor[:,0]])+v[:,1]*np.cos(self.theta()[neighbor[:,0]]))
            return v,d
        else:
            return v

    def plot(self, ax=None, par : dict=None):
        """ plot the cortex """
        print(""" plot the cortex """)
        if par==None: 
            #global cortex_plot_par_defaults
            par=cortex_plot_par_defaults()
        if ax is None: 
            fig, ax= plt.subplots()
        nodes = self.xy().reshape(-1,1,2)
        segments = np.concatenate([nodes[:-1], nodes[1:]], axis=1)
        if self.cortex_par.get('boundary_conditions')=='periodic':
            segments=np.append(segments,np.array([[nodes[-1][0],nodes[0][0]]]),axis=0)
        if 'plot_field' in par.keys() and par['plot_field'] is not None:
            if type( par['plot_field'])==str and par['plot_field']=='alpha': 
                plot_field=self.alpha()
                #print(min(1-1e-6,plot_field.min()), 1,max([1+1e-6,plot_field.max()]))
                norm_colors = pltcolors.TwoSlopeNorm(vmin=min(1-1e-6,plot_field.min()), vcenter=1,vmax=max([1+1e-6,plot_field.max()]))
            else:
                if type( par['plot_field'])==str and par['plot_field']=='theta': plot_field=self.theta()
                elif type( par['plot_field'])==str and par['plot_field']=='gamma': plot_field=self.gamma()
                else: plot_field=par['plot_field']
                norm_colors = plt.Normalize(plot_field.min(),plot_field.max())
            lc=LineCollection(segments,cmap=par['colormap'],norm=norm_colors,linewidths=par['width'])
            lc.set_array(plot_field)
            #if par['draw_colorbox']:
            #    ax.colorbar(label=par['plot_field'])
        else:
            lc=LineCollection(segments,colors=par['color'],linewidths=par['width'],alpha=par['opacity'])
        cortex_plot=ax.add_collection(lc)
        #ax.plot(self.x(),self.y())
        if par['equal_axes']:
            ax.set_aspect('equal', 'box')
        return cortex_plot
