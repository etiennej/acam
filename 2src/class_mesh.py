#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) jan 2024 -- present
# =============================================================================
#
# 1D [periodic] mesh toolbox 
#

import numpy as np 
from scipy.sparse import *  # require version 1.10.1
from scipy.interpolate import interp1d
from scipy.integrate import solve_bvp, cumulative_trapezoid
import itertools

do_mesh_test=False
if do_mesh_test:
    import matplotlib.pyplot as plt


class Mesh(object):
    """
    Attributes
    ---
    N : int
        mesh size - not counting possible periodic endpoint
    periodic : bool
        whether periodic
    _s : np.array(N+periodic)
        curvilinear coordinate - with periodic endpoint if periodic

    Methods
    ---
    __init__(self, N : int =None, periodic : bool=False, s_range : tuple=None, s : array[float] =None)
        constructor, either of a linspace(0,s_max,N) or based on existing s
    N_total(self) -> int
        total number of points, including potential duplicated endpoint
        N_total = N + periodic
    get_s(self) -> array[float]
        returns the curvilinear coordinate of all nodes
        if periodic, the endpoint is only returned as s[0]=0
    get_s_with_periodic_endpoint(self) -> array[float]
        returns the curvilinear coordinate of all nodes
        if periodic the endpoint being duplicated as s[0]=0, s[N]=L where L is the total length
    get_h(self) -> array[float]
        return the vector of element size
    get_h_avg(self) -> array[float]
        returns the Voronoi cell size around each node
    """

    def __init__(self, N=None, periodic=False, s_range=None, s=None):
        self.periodic=periodic
        if s is None:
            if N==None: self.N=100 
            else: self.N=N
            if s_range==None: s_range=(0,1)
            self._s=np.linspace(*s_range, self.N+self.periodic)
        else:
            self._s=s
            self.N=s.size-self.periodic
            if N!=None: assert self.N==N, "Inconsistent sizes"
        self._h=None
        self.interpolated_functions=dict()

    def N_total(self):
        return self.N + self.periodic

    def get_s(self):
        return self._s[:self.N]

    def get_s_with_periodic_endpoint(self):
        return self._s

    def get_h(self):
        """
        h : np_array(N-1+periodic)
            return the vector of element size
        """
        if self._h is None: self._h = self._s[1:]-self._s[:-1]
        return self._h
            
    def get_h_avg(self):
        """
        h_avg : np.array(N)
            returns h_avg[i] = (h*[i-1]+h*[i])/2
            with h*[i] = h[i] for 0 ≤ i < N, h*[-1]=0, h*[N]=0 if not periodic
            and  h*[i] = h[i mod N] if periodic
        """
        if self._h==None: self.get_h()
        if self.periodic:
            return   np.concatenate([[self._h[0]+self._h[-1]], self._h[1:]+self._h[:-1]])/2
        else: return np.concatenate([[self._h[0]], self._h[1:]+self._h[:-1], [self._h[-1]]])/2

    # Deprecated: use Field instead. 
    #
    def on_grid(self, new_s, f):
        """
        on_grid(self, new_s, f) -> array[float]
            interpolates a function f tabulated on s onto new_s
            the interpolant of f is stored in a dictionary to speed up multiple calls
        """
        if new_s is self._s: return f
        if self.periodic and new_s.size == self._s.size-1 and new_s[-1]==self._s[-2]:
            raise Exception("Periodic without boundary point???")
        if id(f) not in self.interpolated_functions:
            self.interpolated_functions[id(f)]=interp1d(self._s, f)
        return self.interpolated_functions[id(f)](new_s)

def test_for_bvp(N, periodic=True, rhs=None):
    def bc (ua, ub):
            if periodic: return ua-ub
            else: return ua
    def psi(s, u):
           return np.array([ -u[0]+mesh.on_grid(s, tabulated_rhs) ]) 
    mesh = Mesh(N=N, periodic=periodic, s_range=[0,2*np.arccos(-1.)])
    if rhs==None: rhs=np.sin
    tabulated_rhs = rhs(mesh.get_s_with_periodic_endpoint())
    y = np.array([tabulated_rhs])
    print(y.shape, tabulated_rhs.shape, mesh.get_s_with_periodic_endpoint().shape)
    sol = solve_bvp(psi, bc, mesh.get_s_with_periodic_endpoint(), y, verbose=2, tol=1e-6)
    print(sol)
    return sol.x,sol.y

if do_mesh_test:
    x,y=test_for_bvp(30, True, np.cos)
    yex=(np.sin(x)+np.cos(x))/2
    print(x.shape,y.shape,yex.shape)
    fig = plt.figure(figsize=(10,7))
    plt.plot(x,yex)
    plt.plot(x,y[0],'.')
    plt.show()

