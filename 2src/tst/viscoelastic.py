#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

"""
Test case: vertical Maxwell fluid thread submitted to a regularized step stress at its end
---
Will validate:
* analytical comparison for viscoelastic relaxation
"""



import sys,os
# allow to find classes in the parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from class_cortex import *
from common_utils import *
import time
import datetime
try:
    import cpuinfo
    mycpuinfo = cpuinfo.get_cpu_info()['brand_raw']
except:
    mycpuinfo = "Unknown CPU -- install py-cpuinfo to record it"
try:
    import sympy
    sympy_installed=True
except:
    sympy_installed=False


def clamped_bc(psiup):
    """
        x_a=0, y_a=L0, θ_a=-π/2
    """
    global L0
    return [get_x(psiup), get_y(psiup)-L0, get_theta(psiup)+np.pi/2]

def load_bc(psidown):
    """
       E(α-1) = ΕF_0 at S=1, and no torque:
       α = F_0 + 1, θ_a'=0, θ_a''=0
    """
    global F0
    return [get_alpha(psidown)-1-F0, get_curvature(psidown), get_diff_curvature(psidown)]

# Tolerance of non-regression
tol=1e-2
# Set to False to run test.
# Set to True to regenerate non-regression test results. /!\
build_test=False
if build_test:
    yes = input("Confirm recalculation of nonregression test results. yes/[no]:")
    if yes!='yes':
        print("Running as a test, remember to turn build_test=False in the code")
        build_test=False

kappa=1 # indifferent
L0 = 1.
F0_list	=	[	.05,	.05,	.05,	.05,	.5	]	#	force	applied	from	t=tau_e	to	t=t1
t1_list	=	[	1.,	1.,	5.,	1.,	.1	]
tau_e_list =	[	.05,	.05,	.05,	.05,	.05	]
inv_tau_c_list =[	1.,	30.,	30.,	3,	30	]	#	inverse	of	the	relaxation	time
Nsteps_list=	[	2000,   400,	2000,	100,	100	]


if build_test:
    cases = []
    for F0,t1,tau_e,inv_tau_c,Nsteps in zip(F0_list,t1_list,tau_e_list,inv_tau_c_list,Nsteps_list):
            cases += [ { 'F0':F0, 't1':t1, 'tau_e':tau_e, 'inv_tau_c':inv_tau_c, 'Nsteps':Nsteps,
                         'cpu': mycpuinfo, 'date':str(datetime.datetime.today()) } ]
else:
    cases = np.load("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], allow_pickle=True)

logger=Logger(logger_normal_mode())
success=0
perf=[]
for case in cases:
    print("Running ", case)
    F0_until_t1=case['F0'] # will be made 0 at t1
    cortex_par=cortex_par_defaults()
    cortex_par["elastic_tolerance"]=1e-3
    cortex_par["boundary_conditions"]='custom'
    cortex_par["inv_tau_c"]=case['inv_tau_c']
    cortex_par["kappa"]=kappa
    cortex_par["logger"]=logger
    cx = Cortex({'shape':'segment', 'from':(0,L0), 'to':(0,0), 'N':100}, cortex_par)
    cx.set_custom_separate_bc(clamped_bc,load_bc)

    F0 = case['F0']
    Nsteps=case['Nsteps']
    t1=case['t1']
    tau_e=case['tau_e']
    tfinal=1.2*t1
    Nsteps_short=np.max([20,Nsteps//40,4*int(Nsteps*tau_e/t1)])
    
    Nsteps_short=np.max([20,Nsteps//40,4*int(Nsteps*tau_e/t1)])

    tfinal=1.2*t1+tau_e
    tswitch=[0,tau_e,t1,t1+tau_e,tfinal]
    trange_list = [ np.linspace(0,tau_e,Nsteps_short),
      np.linspace(tau_e,t1,Nsteps),
      np.linspace(t1,t1+tau_e,Nsteps_short),
      np.linspace(t1+tau_e,tfinal,Nsteps_short) ]
    trange=np.concatenate([tr[1:] for tr in trange_list])

    tprev=0
    deflection=[0]
    cpu_t0=time.process_time()
    for t in trange:
        if t<tau_e: F0=F0_until_t1*(t/tau_e)
        elif t<t1:  F0=F0_until_t1
        elif t<t1+tau_e: F0=F0_until_t1*(1+(t1-t)/tau_e)
        else: F0=0  #if t>t1+tau_e:
        res=cx.elastic_equilibrium()
        cx.relax(t-tprev)
        tprev=t
        deflection += [ -cx.y()[-1] ]
        if res!=0: print("No convergence, exiting"); exit(1)
    cputime=time.process_time()-cpu_t0
    success_status = res 

    if res==0:
        if sympy_installed:
            import itertools
            sym_t=sympy.symbols('t')
            F0=case['F0']
            inv_tau_c=case['inv_tau_c']
            E=1
            theoretical_epsdot = \
             [ F0*(  1 + inv_tau_c*sym_t )/( E*tau_e + F0*sym_t ), 
               F0*inv_tau_c/( E + F0 ),
               F0*( -1 + inv_tau_c*(t1+tau_e - sym_t) )/( E*tau_e + F0*(t1+tau_e - sym_t)),
               0 ]
            theoretical_L = np.array([ sympy.exp(sympy.integrate(epsdot, (sym_t,tmin,sym_t))) for epsdot,tmin in zip(theoretical_epsdot,tswitch[:4])])
            theoretical_L_max = np.array([ L.subs(sym_t,tmax) for L,tmax in zip(theoretical_L,tswitch[1:]) ])
            # initial condition:
            theoretical_L = theoretical_L * np.cumprod( np.append([L0], theoretical_L_max[:-1]) )
            theory_L_of_t =(( [ sympy.lambdify(sym_t,L)(t[1:]) for L,t in zip(theoretical_L,trange_list) ] ))
            theory= [ 0 ]
            for th,t in zip(theory_L_of_t,trange_list): 
                if np.isscalar(th): theory+= (len(t)-1)*[ th-L0 ]   ## if L was a constant, then lambdify hasn't worked...
                else: theory+= list(th-L0)
            max_error=np.max(np.abs(np.array(theory)-np.array(deflection)))/np.max(np.abs(np.array(theory)))
            if max_error>.1:
                print(np.max(np.abs(np.array(theory)-np.array(deflection))), np.max(np.abs(np.array(theory))) )
                print(np.array(theory),np.array(deflection))
        else:
            max_error=-1

    if build_test:
        if res==0:
            if max_error!=-1: case['max_error']=max_error
            else:
                print("Please install sympy to build test")
                exit(1)
            case['deflection']=deflection[-1]
            case['cputime']=cputime
            case['success']=True
        else:
            case['success']=False
        print(case)
    else:
        passed=True 
        if case['success']:
            if res>0:
                print ("> Not converging when it used to.\n",case)
                passed=False
            if max_error>-1:
                if abs(max_error-case['max_error']) > tol*case['max_error']: 
                    print ("> Error ", max_error, " larger than tolerance.\n",case)
                    passed=False
            if abs(deflection[-1]-case['deflection']) > tol*case['deflection']:
                print("Error ", deflection[-1]-case['deflection'], " on final deflection larger than tolerance.\n",case)
                passed=False
            perf+=[ cputime/case['cputime'] ]
            success+=passed

if build_test:
    np.save("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], cases)
else:
    target = sum([case['success'] for case in cases])
    print("Could run %d tests out of %d, out of which %d successful and median relative performance %f" 
        % (len(perf), target, success, np.median(perf)))
    if success<target:
       print("Not all tests succeeded, marked as failed")
       exit(1)
    if np.median(perf)>1.1:
       print("Performance unexpectedly poor, marked as failed")
       exit(1)
