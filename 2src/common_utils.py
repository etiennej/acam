import numpy as np
#import dill
from class_cortex import *
#import warnings

import logging
import argparse
from datetime import datetime

class Case(object):
    """
    Class that handles the specifics of the run.

    Attributes
    ----------
    name : str
        name of the code
    description : str
        brief description of the physics
    runname : str
        name of the run (user-chosen)
    loglevel : int
        level of logging, e.g. logging.INFO for info only, logging.DEBUG for info and debug

    """
    def __init__(self,name_,description_="Guess what it does!"):
        self.name=name_
        self.description=description_
        self.parser = argparse.ArgumentParser(prog = self.name,
            description = self.description)
        self.parser.add_argument("-d", "--debug", action='store_const',
            const=True, default=False, dest="debug",
            help = "set log level to debug mode")
        self.parser.add_argument("--loglevel", type=str, 
            dest = 'loglevel', default = logging.INFO,
            help = "allows fine tuning of logging level")
        self.parser.add_argument("--runname", type=str, 
            dest = 'runname', default = datetime.now().strftime("%y%m%d_%H%M"),
            help = "set the name of the run")
        self.args = self.parser.parse_args()
        self.logger = logging.getLogger(self.args.runname)
        #set logging level
        self.logger.setLevel(self.args.loglevel)
        handler = logging.FileHandler(self.args.runname+".log")
        # create a logging format
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s \n\t %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        #write a debug message
        #logger.debug('This is a DEBUG message, shown in debug mode')
        #logger.info('This is an INFO message, shown in debug or info mode')
        #logger.warning('This is a W message, shown in warning, debug or info mode')

    def figname(self,instant:str):
        return instant+".png"

    def the_force_center_to_zero(self,s,x,z):
        Fx = 0
        Fy = -self.intensity_center_to_zero*np.sum(z)
        return (np.array([Fx,Fy])*np.ones((len(s),2))).T

    def force_center_to_zero(self,intensity):
        self.intensity_center_to_zero=intensity
        return self.the_force_center_to_zero
 
del0=2.e-1
tol=del0/4
reg=0
omega=1e-2
def adhesion_on_plane(s,x,z):
    """
    derivative of energy potential
        dW/dz(z), with W(z) = ( (del0/z)**2-1)**2          
        note that argmin W = del0
        see Sukuruman & Seifert. Phys. Rev. E, 64:011916, 2001
    """
    global del0,reg,omega,tol
    Fx = -1e-4*np.sum(x)
    z[z<=tol]=tol
    return np.array([Fx*np.ones(len(s)), -omega*(4*del0**2*(z-del0)*(z+del0))/z**5])
 
def gravity(s,x,z):
    """
    derivative of energy potential
        dW/dz(z), with W(z) = ( (del0/z)**2-1)**2          
        note that argmin W = del0
        see Sukuruman & Seifert. Phys. Rev. E, 64:011916, 2001
    """
    global del0,reg,omega,tol
    Fx = -1e-4*np.sum(x)
    Fy = -omega
    return (np.array([Fx,Fy])*np.ones((len(s),2))).T

