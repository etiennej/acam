#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

# Compute the equilibrium shape of a horizontal beam under its own weight.
kappa=1e-1
g=.01
# Frictional damping is used and reduced gradually to 0. 

from class_cortex import *
from common_utils import *
import time

def gravity(grid,x,z):
    global omega
    return [np.zeros_like(x), -omega*np.ones_like(z)]


def clamped_bc( psiup):
    """
        x_a=0, y_a=0, θ_a=0
    """
    return [get_x(psiup), get_y(psiup), get_theta(psiup)]

def free_bc(psidown):
    """
        α_b=1, θ_b*=0, θ_b**=0
    """
    print(".",end='')
    return [ get_alpha(psidown)-1, get_curvature(psidown), get_diff_curvature(psidown)]


plt.style.use('dark_background')
fig, ax= plt.subplots()

cortex_par=cortex_par_defaults()
cortex_par["boundary_conditions"]='custom'
cx = Cortex({'shape':'segment', 'from':(0,0), 'to':(1,0), 'N':100}, cortex_par)
cx.add_external_force(gravity)
cx.set_custom_separate_bc(clamped_bc,free_bc)

#cx.cortex_par['analytical_Jacobian'] = True

t0=time.process_time()
for omega,damping in zip(np.linspace(g,g,21),[*np.logspace(2/5, -4, 20),0]):
    cx.cortex_par["kappa"]=kappa
    cx.cortex_par["frictional_damping"]=damping/100
    print(cx.cortex_par)
    res=cx.elastic_equilibrium()
    E_el = cx.elastic_energy()
    E_bg = cx.bending_energy()
    print("E_el = ", E_el, " E_bg = ", E_bg, " tot = ", E_bg+E_el)
    print("Δα=",cx.alpha().max()-cx.alpha().min(),cx.alpha().max(),cx.alpha().min(), "E_f = ", cx.friction_E, "ends=", cx.xy()[0], cx.xy()[-1])
    if res!=0: print("No convergence, exiting"); exit(1)
print("Calculation time",time.process_time()-t0)


cortex_plot_par = cortex_plot_par_defaults()
cortex_plot_par['width']=9
cortex_plot_par['equal_axes']=False
cortex_plot_par['plot_field']='alpha'
cortex_plot_par['color']='r'
cortex_plot=cx.plot(ax,par=cortex_plot_par)
plt.plot([cx.x()[-1]],[cx.y()[-1]],"o")
print("Deflection:",cx.y()[-1])
#print("Contour length:",np.trapz(cx.alpha(),cx.S0))

padding=max(np.max(cx.x())-np.min(cx.x()),np.max(cx.y())-np.min(cx.y()))*.05
ax.set_xlim(np.min(cx.x())-padding,np.max(cx.x())+padding)
ax.set_ylim(np.min(cx.y())-padding,np.max(cx.y())+padding)
#ax.set_ylim(-.1,np.max(cx.y())+padding)
ax.relim()
fig.colorbar(cortex_plot, label=cortex_plot_par['plot_field'])
plt.show()
