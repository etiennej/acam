#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) may 2023 -- present
# =============================================================================
import numpy as np

class Epithelium(object):
    """
    """
    def __init__(self,cortex_list):
        self.size=len(cortex_list)
        self.cortex=np.array(cortex_list)


