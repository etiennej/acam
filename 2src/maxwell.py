#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

# Compute the equilibrium shape of a 1D segment of Maxwell fluid under step stress
kappa=1 # indifferent
L0 = 1.
F0_until_t1 = .1    # force applied from t=0 to t=t1
t1 = 1.
inv_tau_c=3 # inverse of the relaxation time
Nsteps=1000
F0=F0_until_t1 # will be made 0 at t1

from class_cortex import *
from common_utils import *
import time

# doesn't work because of the cx...??
def clamped_bc(psiup):
    """
    x_a=0, y_a=L0, θ_a=-π/2
    """
    global L0
    return [get_x(psiup), get_y(psiup)-L0, get_theta(psiup)+np.pi/2]

def load_bc(psidown):
    """
       2E(α²-1) = ΕF_0 at S=1, and no torque:
       α² = F_0 + 1, θ_a'=0, θ_a''=0
    """
    global F0
    return [get_alpha(psidown)-np.sqrt(1+F0), get_curvature(psidown), get_diff_curvature(psidown)]

plt.style.use('dark_background')
fig, ax= plt.subplots()

cortex_par=cortex_par_defaults()
cortex_par["elastic_tolerance"]=1e-3
cortex_par["boundary_conditions"]='custom'
cortex_par["inv_tau_c"]=inv_tau_c
cx = Cortex({'shape':'segment', 'from':(0,L0), 'to':(0,0), 'N':100}, cortex_par)
cx.cortex_par["kappa"]=kappa
cx.set_custom_separate_bc(clamped_bc,load_bc)
print("ends=", cx.xy()[0], cx.xy()[-1], "boundary conditions (matched already when 0:)")
cx.test_custom_bc(cx.psi[:,0], cx.psi[:,-1])

cortex_plot_par = cortex_plot_par_defaults()
cortex_plot_par['width']=9
cortex_plot_par['equal_axes']=True
cortex_plot=cx.plot(ax)
plt.plot([cx.x()[-1]],[cx.y()[-1]],"o")
print("Deflection:",cx.y()[-1])
print("Contour length:",np.trapz(cx.alpha(),cx.mesh._s))

#cx.cortex_par['analytical_Jacobian'] = True

tfinal=1.2*t1
trange=np.append(np.linspace(0,t1,Nsteps),np.linspace(t1,tfinal,Nsteps//5)[1:])
deflection=[0]
tprev=-tfinal/Nsteps

cpu_t0=time.process_time()
for t in trange:
    if t>t1: F0=0
    print(cx.cortex_par)
    res=cx.elastic_equilibrium()
    print("before: S0=",cx.mesh._s[-1]," psi=",cx.psi[:,-1])
    cx.relax(t-tprev)
    tprev=t
    print("end: S0=",cx.mesh._s[-1]," psi=",cx.psi[:,-1])
    deflection += [ -cx.y()[-1] ]
    if res!=0: print("No convergence, exiting"); exit(1)
print("Calculation time",time.process_time()-cpu_t0)
deflection=np.array(deflection)

cortex_plot_par = cortex_plot_par_defaults()
cortex_plot_par['width']=9
cortex_plot_par['equal_axes']=True
cortex_plot=cx.plot(ax)
plt.plot([cx.x()[-1]],[cx.y()[-1]],"o")
print("Deflection:",cx.y()[-1])
print("Contour length:",np.trapz(cx.alpha(),cx.mesh._s))

padding=max(np.max(cx.x())-np.min(cx.x()),np.max(cx.y())-np.min(cx.y()))*.05
ax.set_xlim(np.min(cx.x())-padding,np.max(cx.x())+padding)
ax.set_ylim(np.min(cx.y())-padding,np.max(cx.y())+padding)
#ax.set_ylim(-.1,np.max(cx.y())+padding)
ax.relim()
fig.colorbar(cortex_plot, label=cortex_plot_par['plot_field'])

Theta=0
Theta1=0
E=1
theory=[0] \
    +list( L0*( (1+F0_until_t1/(2*E+Theta*F0_until_t1))*np.exp(inv_tau_c*F0_until_t1*np.linspace(0,t1,Nsteps)/(2*E+Theta*F0_until_t1)) - 1 ) ) \
    +list( L0*( 1-F0_until_t1/(2*E+Theta1*F0_until_t1) )*( (1+F0_until_t1/(2*E+Theta*F0_until_t1))*np.exp(inv_tau_c*F0_until_t1*np.linspace(t1,t1,Nsteps//5)[1:]/(2*E+Theta*F0_until_t1)) ) - L0 ) 
theorylin=[0] \
    +list( L0*F0_until_t1/(2*E)*(inv_tau_c*np.linspace(0,t1,Nsteps) + 1))   \
    +list( L0*F0_until_t1/(2*E)*inv_tau_c*(np.linspace(t1,t1,Nsteps//5)[1:]) )
#theory=[0] \
#    + list(L0*F0_until_t1 + L0*(1+F0_until_t1)*(np.exp(inv_tau_c*F0_until_t1*np.linspace(0,t1,Nsteps))-1)) \
#    + list(L0*(1+0*F0_until_t1)*(np.exp(inv_tau_c*F0_until_t1*np.linspace(t1,t1,Nsteps//5)[1:])-1))
fig_def, ax_def= plt.subplots()
plt.plot([0]+list(trange), deflection, ".")
plt.plot([0]+list(trange), np.transpose([theory,theorylin]))
#plt.plot([0]+list(trange), theorylin)
#plt.plot([0]+list(trange), np.transpose([deflection-theory,deflection-theorylin]))


print("Final relative error:", (theory[-1]-deflection[-1])/theory[-1])

plt.show()
