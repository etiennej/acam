#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) may 2023 -- present
# =============================================================================
import numpy as np
from scipy.sparse import *  # require version 1.10.1
from scipy.interpolate import interp1d
import itertools

from class_log import *

class force_displacement_linear(object):
    def __init__(self, stiffness):
        self.k = stiffness
        return
    def is_linear(self): return True
    def calculate(self, u, d=-1):
        """
        return the force vectors
        ---
        Input:
            u : array of shape(N,2)
                displacement vectors
            d : optional norm of the vectors
        Output:
            f : array of shape(N,2)
        """
        return self.k*u
    def calculate_xy(self, u, d=-1):
        """
        same as calculate but with transposed vector organisation
        arrays have shape (2,N) and can be lists
        """
        return self.k*np.array(u)
    def grad(self, v=0, d=0):
        return self.k*np.eye(2)

def adhesion_par_defaults():
    return { 'max_radius':1., 'force_displacement_relation':force_displacement_linear(1.), 'regridding_rule':'energy_preserving_adhesion', 'logger':Logger(logger_debug_mode()) }

def adhesion_plot_par_defaults():
    """
    Plot options

    plot_type: 
        List of features to plot
        (Not necessarily already) available values:
        'forces', 'links', 'vectors'.

    color:
        Matplotlib colour

    force_scaling_factor:
        scaling factor (inverse of Matplotlib `scale`)

    force_scale_unit:
        among 'relative' or any of 'xy', 'dots', 'inches', see Matplotlib Quiver documentation for those.
        'relative' means that largest force vector will be 100*force_scale pixel long.
    """
    return { 'plot_type':['forces'], 'color':'#BBBBBB', 'force_scale_units':'relative', 'force_scaling_factor':1 }

class Sparse_Multifield(object):
    """
    A sparse array class representing a collection of sparse 2D data fields of identical sparsity

    Sparse_Multifield is based on:
        - a sparse array of integers k(i,j), represented by different sparse formats for efficiency,
        - a dictionary d of (full) data vectors, such that for a given field, field[i,j]=d['field'][k(i,j)]
    It provides efficient routines to:
        - build the dataset from sparse row-organised data,
        - expand the dataset with new fields with identical sparsity,
        - update the data,
        - access the data by slicing (rows or columns)

    Warning: explicit 0 is not handled reliably by scipy.sparse. Thus data vectors in d are size N+1 and have irrelevant [0] indexed value
    ---
    Methods
    ---
    insert(int:S1,int:S2,val)
        nb: erases existing csr/csc
    delete(int:S1,int:S2)
        nb: erases existing csr/csc
    row(int:S1)
        nb: creates csr if needed
    col(int:S2)
        nb: creates csc if needed
    set_field_data(str:name, values)
        create or reassign values to a field
    ---
    Accessors
    ---
    dok() : data field indices (starting with offset=1) in a dictionary of keys sparse array
        Used for building/updating the matrix
        Data stored in protected _dok, Valid only if has_dok is true
    csr() : same indices in a compressed sparse row array
        Data stored in protected _csr, valid only if has_csr is true
    csc() : same indices in a compressed sparse column array
        Data stored in protected _csc, valid only if has_csc is true
    get_field_data(name="", s1, s2) : returns a dictionary of fields or field name.
    ---
    Data
    ---
    _field_data : (protected) dictionary of fields
        First value is nan because scipy.sparse doesn't handle reliably explicit zero values.
        Cannot be accessed and modified directly as a resulkt of this silly unfixed bug of them.
    ---
    Constants
    ---
    offset: int = 1
        to avoid using 0 which is considered as empty
    """
    offset = 1
    def __init__(self, list_of_lists_of_indices=None, shape=None, csr=None, dok=None, log=None):
        if log==None:
            self.logger = Logger(logger_normal_mode())
        else:
            self.logger = log
        self._field_data = dict()
        if list_of_lists_of_indices is not None:
            indptr = np.append(0, np.cumsum([len(l) for l in list_of_lists_of_indices]))
            csr_indices = np.fromiter(itertools.chain.from_iterable(list_of_lists_of_indices),int)
            data_indices = np.arange(len(csr_indices))+self.offset
            self.__init__(csr=csr_array((data_indices,csr_indices,indptr),shape=shape))
        elif csr is not None:
            self._csr=csr
            self.has_dok = False
            self.has_csr = True
            self.has_csc = False
        elif dok is not None:
            self._dok=dok
            self.has_dok = True
            self.has_csr = False
            self.has_csc = False
        elif shape is not None:
            self._dok = dok_array(N,M)
            self.has_dok = True
            self.has_csr = False
            self.has_csc = False
        else: error

    def csr(self):
        if self.has_csr: return self._csr
        self.has_csr = True
        if self.has_dok: 
            self._csr = self._dok.tocsr()
            return self._csr
        if self.has_csc: 
            self._csr = self._csc.tocsr()
            return self._csr
        raise Exception("No array to convert")

    def dok(self):
        if self.has_dok: return self._dok
        self.has_dok = True
        if self.has_csr: 
            self._dok = self._csr.todok()
            return self._dok
        if self.has_csc: 
            self._dok = self._csc.todok()
            return self._dok
        raise Exception("No array to convert")

    def nnz(self):
        if self.has_dok: return self._dok.nnz
        if self.has_csr: return self._csr.nnz
        if self.has_csc: return self._csc.nnz
        raise Exception("No array present")

    def row_indices(self, s1):
        return self.csr()[[s1],:]

    def regridding_rule_density_conservation(self, idx, field_name, fields):
        return np.sum(fields[field_name][idx])

    def regrid_rows(self,new_indices,new_row_number,rule_for_fields):
        old_shape=self.dok().shape
        self.has_csr=False
        self.has_csc=False
        new_shape=(new_row_number,old_shape[1])
        new_dok = dok_array(new_shape, dtype=int)
        already = {}
        new_k=self.offset
        data_map = {}
        # create new dok with brand new numbering, and lists of data indices corresponding to these new entries
        for ijk in self.dok().items():
            i=ijk[0][0]
            j=ijk[0][1]
            #print (new_indices,j)
            if (new_indices[i],j) not in already: # 'already' set is not necessary...
                new_dok[new_indices[i],j]=int(new_k)            
                already[(new_indices[i],j)]=new_k
                data_map[new_k]=[ijk[1]]
                #print("add ",(new_indices[i],j),new_k)
                new_k+=1
            else:
                data_map[already[(new_indices[i],j)]]+=[ijk[1]]                
        # create new data fields
        new_data_size=new_k
        new_field_data = {}
        for field_name in self._field_data.keys():
            if field_name not in rule_for_fields:
                self.logger.algorithmics(2,"Warning: discarding field `%s' since I have no instruction what to do with it"
                                            % field_name)
            else:
                # create empty array with same shape as previous, except along axis 0
                new_field_data[field_name]=np.empty((new_data_size,*self._field_data[field_name].shape[1:]))
                for new_k in np.arange(self.offset,new_data_size):
                    new_field_data[field_name][new_k] = rule_for_fields[field_name](data_map[new_k],field_name,self._field_data) 
        self.__init__(dok=new_dok)
        self._field_data=new_field_data

    def list_field_data(self): return self._field_data.keys()
    
    def set_field_data(self, name, value):
        if offset!=1: raise Exception("Offset is hardcoded to 1 in this function")
        if np.isscalar(value):
            self._field_data[name]=np.append([np.nan], value*np.ones(self.nnz()),axis=0)
        else:
            self._field_data[name]=np.append(np.nan*np.empty((1,*value.shape[1:])), value,axis=0)

    def get_field_data(self, name="", s1=-1, s2=-1):
        """
        returns field named `name` at indices (s1,s2).
        #If `name` is not provided, returns a dictionary indexed by names.
        If `s2` is not provided, returns a vector of all field values field(s1, . ) -- CPU cost O(1).
        If `s1` is not provided, returns a vector of all field values field(s2, . ) -- CPU cost O(N) upon first access.
        """
        if s1>-1:
            row=self.row_indices(s1)
            if s2>-1:
                if row[:,[s2]].getnnz()>0:
                    idx=row[0,s2]
                    if name=="": return {key:self.field_data[key][idx] for key in field_data}
                    else: return self._field_data[name][idx]
                else:
                    raise Exception("Error, indices %d and %d are not connected" % (s1,s2))
            if name=="": raise Exception("Not coded")
            else: return self._field_data[name][row.data]
        else: raise Exception("Not coded")

    def get_csr_of_field_data(self, name=""):
        """
        return a csr M such that M_ij = field(i,j)
        """
        mycsr=csr_array(self.csr(), copy=True)
        mycsr.data = self._field_data[offset:]
        return mycsr

    def integrate_field_over_S2(self, S2, name=""):
        """
        return a field f(S1) = int field(S1,S2) dS2 
        """
        mycsr = self.get_csr_of_field_data(name)
                


class Adhesion(object):
    """
    Attributes
    ---
    adhesion_par : dict
        parameters
    bonds : dict( (cell1,cell2):Sparse_Multifield )
        with cell1<cell2 (symmetric array)
        contains the data:
        bonds[(cell1,cell2)]["density"] : Sparse field of π_ij density of adhesions between cell1(s_i) and cell2(s_j) 
    """
    def __init__(self, epithelium, adhesion_par=None):
        self.epithelium = epithelium
        n=epithelium.size
        if adhesion_par==None: self.par = adhesion_par_defaults()
        else: self.par = adhesion_par
        self.bonds = dict()
        for pair in itertools.combinations(range(n),2):
            # for large tissue : filter first by bounding box distance
            self.bonds[pair]=self.glue(epithelium.cortex[pair[0]],epithelium.cortex[pair[1]])      
        for cx in epithelium.cortex:
            cx.add_adhesion(self)
    #@#    self.regridding_rule_for_vectors = self.regridding_rule_energy_preserving_vectors

    def glue(self, cx1, cx2):
        """
        Get adhesion data between two cortices as a Sparse_Multifield
        """
        neighbors,distance,u = cx2.vector_to_nearest_within_radius(cx1.xy(), r=self.par["max_radius"])
        # build sparse array corresponding to connectivity
        mf = Sparse_Multifield(neighbors,shape=(len(cx1.xy()),len(cx2.xy())), log=self.par['logger'])
        # flatten lists of lists (but keep 2D vector structure of u)
        mf.set_field_data("distance", np.fromiter(itertools.chain.from_iterable(distance),float))
        mf.set_field_data("u", np.array([uij for ui in u for uij in ui]))
        self.par['logger'].user(0,"Warning. Only pseudo-uniform density coded yet (uniform only if mesh equally spaced)")
        mf.set_field_data("density", 1/mf.nnz())
        return mf

    def get_adhesion_density (self, cortex):
        """
        Calculate and return the field of density of bound linkers on a cortex
        """
        try:
            cortex_index=np.where(self.epithelium.cortex==cortex)[0][0]
        except: raise Exception("cortex %s not in eptihelium %s" % (cortex, self.epithelium)) 
        mycsr=csr_array(self.bonds[(cortex_index,other)].csr(), copy=True)
        mycsr.data = density_data[self.bonds[(cortex_index,other)].offset:]


    def force(self, cortex, cur_S0=-1, cur_xy=-1, restrict_to_pairs=None, retuzorn_gradient=False, return_energy=False):
        """
        PROBABLY ALL WRONG! DOES NOT MAKE A CORRECT INERPOLATION OF FIELDS, INTEGRATE VARIABLES, ETC...
        resultant force density vector at each position

        cortex: forces applied to this cortex by others within `self' will be returned.
        cur_S0: optionnally provide a different curvilinear coordinate grid for cortex, on which forces will be interpolated
        cur_xy: optionnally provide a different xy(S0) shape of cortex which will be taken into account for calculating force
        restrict_to_pairs: list of pairs of cortices to consider, rather than the complete list in `self'
        """
        print("Call force")
        try:
            cortex_index=np.where(self.epithelium.cortex==cortex)[0][0]
        except: raise Exception("cortex %s not in eptihelium %s" % (cortex, self.epithelium)) 
        regridded=False
        add_periodic_end_point=False
        if type(cur_S0)==int: cur_S0=cortex.S0
        elif cur_S0 is not cortex.S0_with_periodic_endpoint and cur_S0 is not cortex.S0: regridded=True
        #        elif len(cur_S0)==len(cortex.S0)-1: ## check whether staggered grid
        #            if abs(cur_S0[0]-np.sum(cortex.S0[:2])/2)>1e-7: 
        #                print("Warning. Mesh adaptation not handled yet", cur_S0[0], "≠", np.sum(cortex.S0[:2])/2)
        #                err=1/0
        #                exit(1)
        #            print("Staggered grid detected")
        #            regridded=True
        #        elif len(cur_S0)==len(cortex.S0)+1: ## check whether periodic end point
        #            if cur_S0[-1]!=cortex.S0_with_periodic_endpoint[-1]: #cur_S0[-2]!=cortex.S0[-1]: 
        #                print("Warning. Mesh adaptation not handled yet", len(cur_S0), len(cortex.S0))
        #                err=1/0
        #                exit(1)
        #            print("Grid with periodic endpoint detected")
        #            add_periodic_end_point=True
        #            periodic_end_point=cur_S0[-1]
        #        elif len(cur_S0)==2*len(cortex.S0)+1: ## check whether periodic end point
        #            if cur_S0[-1]!=cortex.S0_with_periodic_endpoint[-1]: #cur_S0[-3]!=cortex.S0[-1]: 
        #                print("Warning. Mesh adaptation not handled yet", len(cur_S0), len(cortex.S0))
        #                err=1/0
        #                exit(1)
        #            print("Doubled grid with periodic endpoint detected")
        #            add_periodic_end_point=True
        #            regridded=True
        #            periodic_end_point=cur_S0[-1]
        #        elif len(cur_S0)!=len(cortex.S0): ## new grid forbidden
        #            print("Warning. Mesh adaptation not handled yet", len(cur_S0), len(cortex.S0))
        #            print(cur_S0,cortex.S0)
        #            err=1/0
        #            exit(1)
        force_x = np.zeros(cortex.N())
        force_y = np.zeros(cortex.N())
        density = np.zeros(cortex.N())
        # if cur_xy is provided, then the density field needs to be calculated explicitly
        need_density=False
        if not np.isscalar(cur_xy): need_density=True
        e = 0
        linear = self.par["force_displacement_relation"].is_linear()
        if return_gradient:
            if linear:
                grad = self.par["force_displacement_relation"].grad()             
            else:
                raise Exception("Not coded")
        if restrict_to_pairs is None: restrict_to_pairs = self.bonds.keys()
        #for other in np.arange(cortex_index): # for which a transpose of the CSR is needed
        #    raise Exception("Not coded")
        for other in np.arange(cortex_index+1,self.epithelium.size):
            if (cortex_index,other) in self.bonds and (cortex_index,other) in restrict_to_pairs:
                if ("force" in self.bonds[(cortex_index,other)].list_field_data()):
                    # force has been already calculated - below it will be updated if cur_xy is provided
                    force_vector_data=self.bonds[(cortex_index,other)]._field_data["force"]
                    if need_density:
                        density_data = self.bonds[(cortex_index,other)]._field_data["density"]
                else:
                    # data below is the CSR-organised data, only local operations are appropriate
                    density_data = self.bonds[(cortex_index,other)]._field_data["density"]
                    force_vector_data = self.par["force_displacement_relation"].calculate                \
                        (self.bonds[(cortex_index,other)]._field_data["u"], 
                         self.bonds[(cortex_index,other)]._field_data["distance"])
#
#
#
                    raise Exception("PROBLEM HERE ! There should be the length of the element along the other cortex")
                    force_vector_data = (np.array([density_data])).transpose()*force_vector_data
                    self.bonds[(cortex_index,other)]._field_data["force"]=force_vector_data
                # use sparse matrix-vector product to sum by row
                mycsr=csr_array(self.bonds[(cortex_index,other)].csr(), copy=True)
                mycsr.data = force_vector_data[self.bonds[(cortex_index,other)].offset:,0]
                #print("csr_f",mycsr.shape," force_x",force_x.shape)
                force_x += mycsr@np.ones(mycsr.shape[1])
                mycsr.data = force_vector_data[self.bonds[(cortex_index,other)].offset:,1]
                force_y += mycsr@np.ones(mycsr.shape[1])
                if need_density:
                    mycsr.data = density_data[self.bonds[(cortex_index,other)].offset:]
                    density += mycsr@np.ones(mycsr.shape[1])
                if return_energy:
                    e += np.sum(force_vector_data*self.bonds[(cortex_index,other)].field_data["u"])
        force = np.array([force_x, force_y]) #.transpose()
        if not np.isscalar(cur_xy):
            if linear:
                #print(cur_xy.shape, np.array([cortex.x(),cortex.y()]).shape)
                #print(cur_xy[:,0:len(cortex.S0)].shape, np.array([cortex.x(),cortex.y()]).shape)
                #print ("u:", np.max (np.abs([cortex.x() - cur_xy[0][0:len(cortex.S0)], 
                #     cortex.y() - cur_xy[1][0:len(cortex.S0)]]) ))
                #print ("f:", np.max (np.abs(self.par["force_displacement_relation"].calculate_xy ([cortex.x() - cur_xy[0][0:len(cortex.S0)], 
                #     cortex.y() - cur_xy[1][0:len(cortex.S0)]]) )))
                #print ("F:", np.max (np.abs(force)))
                print(len(cortex.S0), cur_xy[0].shape, len(cortex.S0_with_periodic_endpoint))
                force+=density*self.par["force_displacement_relation"].calculate_xy \
                    ([cortex.x() - cur_xy[0][0:len(cortex.S0)], 
                      cortex.y() - cur_xy[1][0:len(cortex.S0)]]) 
            else:
                self.par['logger'].user(0,"Nonlinear force not coded yet")
                exit(1)
        if len(cortex.S0)==len(cortex.S0_with_periodic_endpoint)-1: #should we add periodic_end_point:
            if cur_S0 is not cortex.S0:
                force=np.append(force, np.transpose([force[:,0]]), axis=1)
                S0 = cortex.S0_with_periodic_endpoint
        else:
            S0 = cortex.S0
        if regridded:
            force=interp1d(S0,force)(cur_S0)
        ret = force
        if return_gradient:
            ret=(force,grad)
        if return_energy:
            ret=(ret,e)
        return ret   

    regridding_rule_density_conservation = Sparse_Multifield.regridding_rule_density_conservation 
    
    def regridding_rule_copy_vectors(self, idx, field_name, fields):
        v  = fields[field_name][idx]
        v2 = np.sum(v*v,axis=1)
        n  = fields["density"][idx]
        norm = np.linalg.norm(np.sum(n*v))
        return np.sum(v,axis=0)

    def regridding_rule_energy_preserving_vectors(self, idx, field_name, fields):
        v  = fields[field_name][idx]
        v2 = np.sum(v*v,axis=1)
        n  = fields["density"][idx]
        norm = np.linalg.norm(np.sum(n*v))
        return np.sum(n*v2) * np.sum(n*v) /(np.sum(fields[field_name][idx])*norm)

    def recalculate_distances_from_vectors(self, pairs=-1, overwrite=False):
        if pairs==-1:
            pairs=self.bonds.keys()
        if type(pairs)==tuple:
            pairs=[pairs]
        for p in pairs:
            try:
                u = self.bonds[p]._field_data["u"]
            except: raise Exception("No vector data")
            if "distance" in self.bonds[p].list_field_data() and not overwrite:
                raise Exception("distance data already present")
            self.bonds[p]._field_data["distance"]=np.zeros_like(self.bonds[p]._field_data["density"])
            for i,ui in enumerate(u):
                self.bonds[p]._field_data["distance"][i]=np.linalg.norm(ui)

    def regrid_nearest(self, cortex, old_S0, old_xy):
        self.par['logger'].user(0,"REGRID")
        try:
            cortex_index=np.where(self.epithelium.cortex==cortex)[0][0]
        except: raise Exception("cortex %s not in eptihelium %s" % (cortex, self.epithelium)) 
        for other in np.arange(cortex_index):
            raise Exception("Not coded")
        for other in np.arange(cortex_index+1,self.epithelium.size):
            if (cortex_index,other) not in self.bonds:
                self.par['logger'].algorithmics(2,"Warning in regrid. Cortices weren't glued yet... doing it")
                self.bonds[(cortex_index,other)]=self.glue(cortex,epithelium.cortex[other])      
            else:
                # get closest node on new mesh for each node from old one:
                self_neighbor = cortex.nearest_node(old_xy, n=1)[1].transpose()[0] 
                if self.par["regridding_rule"]=="energy_preserving_adhesion":
                    self.bonds[(cortex_index,other)].regrid_rows(self_neighbor, new_row_number=len(cortex.S0), 
                        rule_for_fields={'density':self.regridding_rule_density_conservation, 'u':self.regridding_rule_for_vectors})
                    self.recalculate_distances_from_vectors(pairs=[(cortex_index,other)]) 
                else: raise Exception("Regridding rule not coded: ", self.par["regridding_rule"])

    def plot(self, ax=None, cortices=None, cortex_pair_list=None, plot_par : dict=None):
        """
        plot adhesion data

        See `adhesion_plot_par_defaults` for the different plotting options.

        If a list of indices is provided in `cortices`, plot only adhesions or forces exerted on these.
        If a list of pairs of indices is provided in `cortex_pair_list`, plot only adhesions or forces exerted between these.
        These options can be combined.
        """
        if plot_par==None: 
            #global cortex_plot_par_defaults
            plot_par=adhesion_plot_par_defaults()
        if ax is None: 
            fig, ax= plt.subplots()
        if cortex_pair_list==None: cortex_pair_list=self.bonds.keys()
        if cortices==None: cortices=np.arange(self.epithelium.size)
        else:
            cortex_pair_list=[p for p in cortex_pair_list if p[0] in cortices or p[1] in cortices]   
        min_distance = np.inf
        max_norm = -np.inf
        adh_plot = []
        force_vectors=[]
        if 'links' in plot_par['plot_type'] or 'vectors' in plot_par['plot_type']:
            for pair in cortex_pair_list:
                idx_pair_list = self.bonds[pair].dok().keys()
                xy1 = self.epithelium.cortex[pair[0]].xy()
                xy2 = self.epithelium.cortex[pair[1]].xy()
                the_xy1 = xy1[[ip[0] for ip in idx_pair_list]]
                the_xy2 = xy2[[ip[1] for ip in idx_pair_list]]
                xy1_to_xy2 = the_xy2-the_xy1
                min_distance = min(min_distance, np.min(np.linalg.norm(xy1_to_xy2,axis=0)))
                if 'links' in plot_par['plot_type']:
                    vectors=np.concatenate([the_xy1, xy1_to_xy2],axis=1).transpose()
                    adh_plot+=[ax.quiver(*list(vectors), color=plot_par['color'], scale_units='xy',scale=1.,headwidth=0,headlength=0)]
        if 'forces' in plot_par['plot_type']:
            for idx_cx in cortices:
                forces = self.force(self.epithelium.cortex[idx_cx], self.epithelium.cortex[idx_cx].S0, restrict_to_pairs=cortex_pair_list)
                force_vectors += [ [self.epithelium.cortex[idx_cx].x(), self.epithelium.cortex[idx_cx].y(), *list(forces)] ]
                #print("fv:",[[ len(f) for f in F ] for F in force_vectors ])
                norms = np.linalg.norm(forces,axis=0)
                max_norm = max(max_norm, np.max(norms))
        #print('n=',max_norm)
        if 'forces' in plot_par['plot_type']:
            for idx_cx in cortices:
                if plot_par['force_scale_units']=='relative':
                    self.force_scale=max_norm/(100*plot_par['force_scaling_factor'])
                    self.force_scale_units='dots'
                else:
                    self.force_scale_units=plot_par['force_scale_units']
                    self.force_scale=1/plot_par['force_scaling_factor']
                for F in force_vectors:
                    adh_plot+=[ax.quiver(*F, color='r', scale_units=self.force_scale_units, scale=self.force_scale)]
        return adh_plot 
