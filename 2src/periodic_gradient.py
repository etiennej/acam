import numpy as np

do_test_gradient = False

def periodic_gradient(f, *varargs, axis=None, periodic_offset=None):
    """
    Return the gradient of an N-dimensional array, assuming periodicity
    of the data.
    Operates along one axis only.
    If an offset is present along it, it should be provided.
    The gradient is calculated via numpy.gradient after padding the vector
    by the periodic data.
    """
    S=2 # stencil size for order 2
    f=np.asanyarray(f)
    assert type(axis) == int or len(f.shape)==1, "periodic_gradient operates along one axis only"  
    if periodic_offset is None: periodic_offset=np.zeros_like(f.take(indices=[0],axis=axis))
    else: periodic_offset=np.asanyarray(periodic_offset)
    if len(f.shape)>1:
        sh=np.array(f.shape)
        sh[axis]=1
        if len(periodic_offset.shape)==len(sh):
            assert (periodic_offset.shape == sh).all(), "shape of periodic offset was expected to be "+str(sh)
        elif len(periodic_offset.shape)==1 and len(f.shape)==2:
            # special case of 2D data allows automatic treatment
            if axis==0: periodic_offset=np.array([periodic_offset])
            else: periodic_offset=np.transpose([periodic_offset])
        else: raise Exception ("shape of periodic offset was expected to be "+str(sh))
    ff=np.concatenate(
        [   f.take(indices=range(-S-1,-1),axis=axis) - periodic_offset,
            f,
            f.take(indices=range(1,1+S),axis=axis) + periodic_offset ],
        axis=axis)
    n = len(varargs)
    if n == 0 or ( n == 1 and np.ndim(varargs[0]) == 0 ):
        return np.gradient(ff,*varargs,axis=axis).take(indices=range(S,ff.shape[axis]-S),axis=axis)
    elif n == 1 and np.ndim(varargs[0]) == 1:
        x = varargs[0]
        xx=np.concatenate([x[-S-1:-1]-x[-1],x,x[1:S+1]+x[-1]])
        return np.gradient(ff,xx,axis=axis).take(indices=range(S,ff.shape[axis]-S),axis=axis)
    else:
        raise TypeError("invalid number of arguments")

def problem_periodic_gradient(f, *varargs, axis=None, periodic_offset=None):
    """
    Author: Jocelyn Etienne
    (CC) October 2022
    Derived from numpy.gradient v1.23.0, 19 May 2022.
    DOESN'T CONVERGE AS FAST ON BOUNDARIES
    Return the gradient of an N-dimensional array, assuming periodicity
    of the data.
    The gradient is computed using second order accurate central differences.
    If an offset is present along some axes, it should be provided.
    The returned gradient hence has the same shape as the input array.
    Parameters
    ----------
    f : array_like
        An N-dimensional array containing samples of a scalar function.
    varargs : list of scalar or array, optional
        Spacing between f values. Default unitary spacing for all dimensions.
        Spacing can be specified using:
        1. single scalar to specify a sample distance for all dimensions.
        2. N scalars to specify a constant sample distance for each dimension.
           i.e. `dx`, `dy`, `dz`, ...
        3. N arrays to specify the coordinates of the values along each
           dimension of F. The length of the array must match the size of
           the corresponding dimension
        4. Any combination of N scalars/arrays with the meaning of 2. and 3.
        If `axis` is given, the number of varargs must equal the number of axes.
        Default: 1.
    axis : None or int or tuple of ints, optional
        Gradient is calculated only along the given axis or axes
        The default (axis = None) is to calculate the gradient for all the axes
        of the input array. axis may be negative, in which case it counts from
        the last to the first axis.
    periodic_offset: None or scalar or list of scalar or array, optional
        ...
    Returns
    -------
    gradient : ndarray or list of ndarray
        A list of ndarrays (or a single ndarray if there is only one dimension)
        corresponding to the derivatives of f with respect to each dimension.
        Each derivative has the same shape as f.
    """
    f = np.asanyarray(f)
    N = f.ndim  # number of dimensions

    if axis is None:
        axes = tuple(range(N))
    else:
        axes = np.core.numeric.normalize_axis_tuple(axis, N)

    len_axes = len(axes)
    n = len(varargs)
    if n == 0:
        # no spacing argument - use 1 in all axes
        dx = [1.0] * len_axes
    elif n == 1 and np.ndim(varargs[0]) == 0:
        # single scalar for all axes
        dx = varargs * len_axes
    elif n == len_axes:
        # scalar or 1d array for each axis
        dx = list(varargs)
        for i, distances in enumerate(dx):
            distances = np.asanyarray(distances)
            if distances.ndim == 0:
                continue
            elif distances.ndim != 1:
                raise ValueError("distances must be either scalars or 1d")
            if len(distances) != f.shape[axes[i]]:
                raise ValueError("when 1d, distances must match "
                                 "the length of the corresponding dimension")
            if np.issubdtype(distances.dtype, np.integer):
                # Convert numpy integer types to float64 to avoid modular
                # arithmetic in np.diff(distances).
                distances = distances.astype(np.float64)
            diffx = np.diff(distances)
            # if distances are constant reduce to the scalar case
            # since it brings a consistent speedup
            if (diffx == diffx[0]).all():
                diffx = diffx[0]
            dx[i] = diffx
    else:
        raise TypeError("invalid number of arguments")

    outvals = []

    # create slice objects --- initially all are [:, :, ..., :]
    slice1 = [slice(None)]*N
    slice2 = [slice(None)]*N
    slice3 = [slice(None)]*N
    slice4 = [slice(None)]*N

    otype = f.dtype
    if otype.type is np.datetime64:
        # the timedelta dtype with the same unit information
        otype = np.dtype(otype.name.replace('datetime', 'timedelta'))
        # view as timedelta to allow addition
        f = f.view(otype)
    elif otype.type is np.timedelta64:
        pass
    elif np.issubdtype(otype, np.inexact):
        pass
    else:
        # All other types convert to floating point.
        # First check if f is a numpy integer type; if so, convert f to float64
        # to avoid modular arithmetic when computing the changes in f.
        if np.issubdtype(otype, np.integer):
            f = f.astype(np.float64)
        otype = np.float64

    for axis, ax_dx in zip(axes, dx):
        if f.shape[axis] < 2:
            raise ValueError(
                "Shape of array too small to calculate a numerical gradient, "
                "at least 2 elements are required.")
        # result allocation
        out = np.empty_like(f, dtype=otype)

        # spacing for the current axis
        uniform_spacing = np.ndim(ax_dx) == 0

        # Numerical differentiation: 2nd order interior
        slice1[axis] = slice(1, -1)
        slice2[axis] = slice(None, -2)
        slice3[axis] = slice(1, -1)
        slice4[axis] = slice(2, None)

        if uniform_spacing:
            out[tuple(slice1)] = (f[tuple(slice4)] - f[tuple(slice2)]) / (2. * ax_dx)
        else:
            dx1 = ax_dx[0:-1]
            dx2 = ax_dx[1:]
            a = -(dx2)/(dx1 * (dx1 + dx2))
            b = (dx2 - dx1) / (dx1 * dx2)
            c = dx1 / (dx2 * (dx1 + dx2))
            # fix the shape for broadcasting
            shape = np.ones(N, dtype=int)
            shape[axis] = -1
            # 1D equivalent -- out[1:-1] = a * f[:-2] + b * f[1:-1] + c * f[2:]
            out[tuple(slice1)] = a * f[tuple(slice2)] + b * f[tuple(slice3)] + c * f[tuple(slice4)]

        # Numerical differentiation: 2nd order periodic edges
        slice1[axis] = 0
        slice2[axis] = -1
        slice3[axis] = 0
        slice4[axis] = 1
        if uniform_spacing:
            out[tuple(slice1)] = (f[tuple(slice4)] - f[tuple(slice2)]) / (2. * ax_dx)
        else:
            dx1 = ax_dx[-1]
            dx2 = ax_dx[0]
            a = -(dx2)/(dx1 * (dx1 + dx2))
            b = (dx2 - dx1) / (dx1 * dx2)
            c = dx1 / (dx2 * (dx1 + dx2))
            # 1D equivalent -- out[0] = a * f[-1] + b * f[0] + c * f[1]
            out[tuple(slice1)] = a * f[tuple(slice2)] + b * f[tuple(slice3)] + c * f[tuple(slice4)]

        slice1[axis] = -1
        slice2[axis] = -2
        slice3[axis] = -1
        slice4[axis] = 0
        if uniform_spacing:
            out[tuple(slice1)] = (f[tuple(slice4)] - f[tuple(slice2)]) / (2. * ax_dx)
        else:
            dx1 = ax_dx[-2]
            dx2 = ax_dx[-1]
            a = -(dx2)/(dx1 * (dx1 + dx2))
            b = (dx2 - dx1) / (dx1 * dx2)
            c = dx1 / (dx2 * (dx1 + dx2))
            # fix the shape for broadcasting
            shape = np.ones(N, dtype=int)
            shape[axis] = -1
            #a.shape = b.shape = c.shape = shape
            # 1D equivalent -- out[1:-1] = a * f[:-2] + b * f[1:-1] + c * f[2:]
            out[tuple(slice1)] = a * f[tuple(slice2)] + b * f[tuple(slice3)] + c * f[tuple(slice4)]

        outvals.append(out)

        # reset the slice object in this dimension to ":"
        slice1[axis] = slice(None)
        slice2[axis] = slice(None)
        slice3[axis] = slice(None)
        slice4[axis] = slice(None)

    if len_axes == 1:
        return outvals[0]
    else:
        return outvals

if do_test_gradient:
    a = 3.
    periodic_offset = a
    oerr=np.nan
    omerr=np.nan
    for iN in range(5):
        N=20*10**iN+1
        x = np.linspace(0,1,N) + np.random.rand(N)/N/2
        x[0]=0
        x[-1]=1
        f = [ np.cos(2*np.pi*x) + a*x, np.sin(2*np.pi*x) - a*x]
        gr_h = periodic_gradient(f,x,axis=1,periodic_offset=[a,-a]) 
        #gr_h = np.gradient(f,x,axis=1, edge_order=2) 
        #gr_h = periodic_gradient(f,1/(N-1),periodic_offset=[a,-a],axis=1)  # ONLY FOR 0 NOISE
        gr_e = np.array([ -2*np.pi*np.sin(2*np.pi*x) + a, 2*np.pi*np.cos(2*np.pi*x) - a])
        err = np.abs(gr_e - gr_h)
        max_err=np.max(err,axis=1)
        l1_err=np.sum(err,axis=1)/N
        l2_err=np.sqrt(np.sum(err**2,axis=1)/N)
        exp=np.log10(oerr/l2_err)
        mexp=np.log10(omerr/max_err)
        np.set_printoptions(formatter={"float_kind": lambda x: "%.2g" % x})
        print("N=",N," l2 err=",l2_err, "order=",exp, "max err=", max_err, " order=", mexp)
        oerr=l2_err
        omerr=max_err
