#!/usr/bin/env python3
# conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) jan 2024 -- present
# =============================================================================
#
# 1D [periodic] finite element field toolbox 
#

do_test_field=False

import numpy as np 
from scipy.sparse import *  # require version 1.10.1
from scipy.interpolate import interp1d
from scipy.integrate import solve_bvp, cumulative_trapezoid
import itertools
from periodic_gradient import *

from class_mesh import *

class Field(np.ndarray):
    """
        array of components, which are arrays of values on the mesh
        shape n × mesh.N
        contains a duplicated endpoint if the mesh is periodic

    ---
    Attributes
    ---
    mesh : Mesh
        the associated mesh (which specifies whether field is periodic or not)
    n : int
        number of components
    _Pf : interp1d
        the interpolant
    _diff : array[array]
        array of the gradient of components
    _ddiff : array[array]
        array of the double gradient of components


    Methods
    ---
    __new__(mesh, nodal_values, periodicity_offset=None, fieldnames=None)
        create a field on the `mesh` from the array `nodal_values`, where `nodal_values.shape` is:
            `(1,M)` or `(M,)`       for a single component field
            `(n,M)`                 for an n-componend field
        where `M` is equal to:
            `mesh.N`                if mesh is not periodic 
            `mesh.N` or `mesh.N+1`  if mesh is periodic 
        In the periodic case, 
            if `M == mesh.N`        then `periodicity_offset` must be provided
            if `M == mesh.N+1`      then `periodicity_offset` will be set automatically
    __new__(mesh, n_comp=1, value=0., periodicity_offset=None, fieldnames=None) 
        create a field on the mesh wih n_comp components and fill them with value 0.

        `fieldnames` is optional and of size n_comp.

        `par` can include:
            `debug` : make consistency tests

    
    """
    def __new__(cls, mesh, nodal_values=None, n_comp=1, value=0.,  periodicity_offset=None, fieldnames=None, par : dict={}):
        """
        Create a new field:
        - from existing array of arrays `nodal_values`, then `n_comp` and `value` are ignored. If `mesh` is periodic, `nodal_values` may contains a 
            column of values for each endpoint or a `periodicity_offset` should be provided
        - or by specifying the number of components `n_comp` or listing them by name `fieldnames`=['first_component', 'second_one', ...] and providing
            an initialisation `value`
        """
        # make arguments consistent
        if nodal_values is not None:
            f=np.asarray(nodal_values)
            if len(f.shape)==1: f=np.array([f])
        if fieldnames!=None:
            if 'debug' in par and par['debug']:
                assert len(fieldnames) == n_comp, "Inconsistent n_comp and fieldnames"
                assert not np.array(["," in s for s in  fieldnames]).any(), "Colons `:` cannot be part of a fieldname"
            n_comp=len(fieldnames)
        else:
            if nodal_values is not None:
                if 'debug' in par and par['debug']:
                    assert n_comp==1 or nodal_values.shape[0] == n_comp, "Inconsistent n_comp and nodal_values"
                n_comp = f.shape[0]
            fieldnames=["@"+str(i) for i in range(n_comp)]

        # build object, this calls __array_finalize__ with a "bare" field
        if nodal_values is None:
            obj = np.full([n_comp, mesh.N_total()], value).view(cls)
            if mesh.periodic:
                if periodicity_offset is not None: obj[:,-1]+=periodicity_offset
                else: periodicity_offset=np.zeros(n_comp)
                obj.periodicity_offset=periodicity_offset
        else:
            if mesh.periodic:
                if f.shape[1] == mesh.N:
                    assert periodicity_offset is not None, "Periodicity offset is needed, use `periodicity_offset=0` for strictly periodic field"
                    if type(periodicity_offset)==int and periodicity_offset==0: periodicity_offset=np.zeros(n_comp)
                    obj = np.append(f, np.transpose([f[:,-1] + periodicity_offset]),axis=1).view(cls)
                elif f.shape[1] == mesh.N_total(): # which is N+1
                    if periodicity_offset is not None and 'debug' in par and par['debug']:
                        assert np.max(np.abs([f[:,-1] - f[:,0] + periodicity_offset]))<1e-7,    \
                            "Periodicity offset %s does not match data's %s" % (periodicity_offset.join(", "), [f[:,-1]-f[:,0]].join(", "))
                    obj = f.view(cls)
                else: raise Exception("Shape of data values "+str(f.shape)+" inadequate for periodic mesh size "+str(mesh.N_total()))
                obj.periodicity_offset=periodicity_offset
            else:
                if 'debug' in par and par['debug']:
                    assert f.shape[1] == mesh.N_total(), "Shape of values "+str(f.shape)+" inadequate for mesh size "+str(mesh.N_total())
                obj = f.view(cls)

        # set the attributes to the values given
        obj.mesh = mesh
        obj.par=par
        obj.fieldnames=dict(zip(fieldnames,range(n_comp)))
        assert obj.shape[-1]==mesh.N_total(), \
                "Wrong data size "+str(obj.shape)+" for mesh of size "+str(mesh.N_total())+", are you attempting to create a `field` from a slice?"
                # ( obj.shape[-1]==mesh.N or obj.shape[-1]==mesh.N_total()), 
        return obj

    def __array_finalize__(self, obj):
        if obj is None: raise Exception('Attempting to create a `None` field')
        # in case obj is a view of another array, get that array as it is the one that should match the mesh:
        # (in particular if obj is a slice: then it may contain fewer points)
        if obj.base is None: base=obj
        else: base=obj.base
        # get mesh from obj
        self.mesh = getattr(obj, 'mesh', None) 
        self.par =  getattr(obj, 'par', dict()) 
        #if 'debug' in self.par and self.par['debug']:
        if len(base.shape)<=1: self.n_comp = len(base.shape) # 0 or 1
        else: self.n_comp = base.shape[0]
        # set calculated data to None
        self._Pf = None
        self._diff = None
        self._ddiff = None
        self._nodval_diff = None
        self._nodval_ddiff = None
        # no attempt to preserve field names
        fieldnames=["@"+str(i) for i in range(self.n_comp)]
        self.fieldnames=dict(zip(fieldnames,range(self.n_comp)))


    #def __len__ (self): return obj.shape[0]

    def __getitem__(self, comp):
        if type(comp) is not slice:  return np.ndarray.__getitem__(self,self.resolve_comp(comp))
        else:  return np.ndarray.__getitem__(self,comp)

    def __call__(self, grid, comp=None):
        """
        Return nodal values interpolated on mesh `new_s`
        """
        if type(self.base)!=np.ndarray:
            assert self.shape[-1]==self.mesh.N_total(), "Slices of Fields cannot be interpolated"
        if grid is self.mesh._s:
            if comp is None: 
                # return a 1D array if there's a single component
                if len(self.shape)>1 and self.shape[0]==1: return np.asarray(self[0])
                else: return np.asarray(self)
            else: return np.asarray(self[comp])
        if self.mesh.periodic and grid.size == self.mesh._s.size-1 and grid[-1]==self.mesh._s[-2]:
            raise Exception("Periodic without boundary point???")
        if self._Pf is None:
            self._Pf=interp1d(self.mesh._s, self)
        if comp is None:
            # return a 1D array if there's a single component
            if len(self.shape)>1 and self.shape[0]==1: return self._Pf(grid)[0]
            else: return self._Pf(grid)
        #elif type(comp)==str:
        #    return self._Pf(grid)[comp]
        else: #type(comp)==int or array of ints
            return self._Pf(grid)[self.resolve_comp(comp)]

    def sub(self, other):
        if self.mesh is not other.mesh:
            raise Exception("Fields should be defined on the same mesh")
        return Field(self.mesh,np.asarray(self)-np.asarray(other),self.par)

    def to_mesh(self, new_mesh):
        fh = Field(new_mesh,self(new_mesh._s),fieldnames=self.fieldnames,par=self.par)
        return fh

    def resolve_comp(self,comp):
        if type(comp)==str: 
            if comp in self.fieldnames: return self.fieldnames[comp]
            elif type(comp) is str and ":" in comp: 
                c0,c1=comp.split(":")
                return slice(self.resolve_comp(c0),self.resolve_comp(c1)+1)
            else: raise Exception("No component `",comp,"` found.")
        return comp

    def nodval(self, comp=None):
        """
        return the vector of nodal values
        """
        if type(self.base)!=np.ndarray:
            assert self.shape[-1]==self.mesh.N_total(), "Slices of Fields along axis 1 cannot return nodal values"
            if len(self.shape)==1 and comp==None: return np.asarray([self])
        if comp==None: 
            if len(self.shape)==1:  return np.asarray([self])
            else: return np.asarray(self)
        else: return np.asarray(self[self.resolve_comp(comp)])

    def nodval_diff(self, comp=None):
        """
        Returns nodal values of the derivative of f[comp] or of f if comp==None
        takes into account periodicity
        stores it
        """
        if self.mesh.periodic: gradient=periodic_gradient
        else: gradient=np.gradient
        if self._nodval_diff is None: self._nodval_diff=gradient(self.nodval(), self.mesh.get_s_with_periodic_endpoint(), axis=1)
        if comp==None:
            return self._nodval_diff
        else:
            comp=self.resolve_comp(comp)
            return self._nodval_diff[comp]

    def diff(self, grid=None, comp=None):
        """
        Returns a field containing the derivative of f[comp] or of f if comp==None, or its interpolation on a grid
        takes into account periodicity
        stores it
        """
        if self._diff is None: self._diff=Field(self.mesh, self.nodval_diff())
        if grid is None:
            if comp==None: return self._diff
            else: return self._diff[comp]
        else:
            if comp==None: return self._diff(grid)
            else: return self._diff(grid,comp)

    def nodval_ddiff(self, comp=None):
        """
        Returns the nodal values of the second derivative of f[comp] or of f if comp==None
        takes into account periodicity
        stores it
        """
        if self.mesh.periodic: gradient=periodic_gradient
        else: gradient=np.gradient
        if self._nodval_ddiff is None: self._nodval_ddiff=gradient(self.nodval_diff(), self.mesh.get_s_with_periodic_endpoint(), axis=1)
        if comp==None:
            return self._nodval_ddiff
        else:
            comp=self.resolve_comp(comp)
            return self._nodval_ddiff[comp]

    def ddiff(self, grid=None, comp=None):
        """
        Returns a field containing the derivative of f[comp] or of f if comp==None
        takes into account periodicity
        stores it in case comp==None
        """
        if self._ddiff is None: self._ddiff=Field(self.mesh, self.nodval_ddiff())
        if grid is None:
            if comp==None: return self._ddiff
            else: return self._ddiff[comp]
        else:
            if comp==None: return self._ddiff(grid)
            else: return self._ddiff(grid,comp)

def interpolate(mh, f_e, fieldnames=None):
    fh = Field(mh, n_comp=len(f_e), fieldnames=fieldnames)
    if type(f_e) is not list: f_e = [ f_e ]
    fh[:] = np.array([ fi_e(mh.get_s_with_periodic_endpoint()) for fi_e in f_e ])
    if mh.periodic: fh.periodicity_offset=fh[-1]-fh[0]
    return fh
    
def join_components(F, G):
    """
    add the components of Fields `F` and `G`
    """
    assert F.mesh is G.mesh, "Gields don't have the same mesh"
    fieldnames=list(F.fieldnames.keys())
    for fi,fn in enumerate(G.fieldnames):
        if fn[0]!="@": 
            assert fn not in F.fieldnames, "Component `"+fn+"` already present in field"
            fieldnames+=[fn]
        else:
            fieldnames+=["@"+str(fi+F.n_comp)]
    vals = np.concatenate([F, G])
    return Field(F.mesh,vals,fieldnames=fieldnames)

def dot(fh: Field, gh: Field = None):
    """
    <fh,gh>_L2 or <fh,fh>_L2 is gh is None
    """
    if gh is None: gh=fh
    assert fh.mesh is gh.mesh, "Fields should be defined on same mesh"
    h_star = np.concatenate([[0], fh.mesh.get_h(), [0]])
    K_diag = (h_star[:-1]+h_star[1:])/3
    K_sudiag = fh.mesh.get_h()/6
    F=fh.nodval()
    G=gh.nodval()
    #print(fh._f.shape,gh._f.shape, K_diag.shape)
    return np.sum(F*K_diag*G,axis=1) + np.sum(F[:,:-1]*K_sudiag*G[:,1:],axis=1) + np.sum(F[:,1:]*K_sudiag*G[:,:-1],axis=1)

def norm2(fh: Field):
    return dot(fh,fh)
def norm(fh: Field):
    return np.sqrt(dot(fh,fh))

if do_test_field:
    oerr=np.nan
    operio_err=np.nan
    oerr2=np.nan
    oerr3=np.nan
    oerr3tol=np.nan
    oerr4=np.nan
    oerr4tol=np.nan
    def derivative_cos(x, y): 
        return [y[1],-y[0],-y[2]]
    def bc_cos_periodic(ya, yb): return [ya[0]-yb[0],ya[0]-1,ya[2]-1]
    def bc_cos_notperiodic(ya, yb): return [ya[0]-1,yb[0]-1,ya[2]-1]
    for iN in range(0,8):
        N=20*2**iN+1
        periodic=True
        if periodic: bc_cos=bc_cos_periodic
        else: bc_cos=bc_cos_notperiodic
        L=2*np.pi
        m = Mesh(N=N, periodic=periodic, s_range=[0,L])
        f = interpolate(m, [np.cos, lambda x: (-np.sin(x)), lambda x: (np.exp(-x))])
        #f_np=[np.array(np.cos(np.linspace(0,2*np.pi, N+2)))]
        #f_np+=[f_np[0]**2]
        #f_np=np.array(f_np)
        #print(f_np.shape)
        err=L/2+np.array([1,-1])*np.sin(2*L)/4-dot(f[:2],f[:2])
        #err_np=np.pi*np.array([1,3/4])-np.sum(f_np**2)/N
        exp=np.log2(oerr/err)
        m2 = Mesh(N=N+1, periodic=periodic, s_range=[0,L])
        f2 = f[:2].to_mesh(m2)
        err2=np.pi +dot(f[:2],f[:2].ddiff())
        exp2=np.log2(oerr2/err2)
        np.set_printoptions(formatter={"float_kind": lambda x: "%.2g" % x})
        print("N=",N," err(<f,f>)=",err, "order=",exp," remesh_err=",L/2+np.array([1,-1])*np.sin(2*L)/4-dot(f2,f2)," <f,f'>=",dot(f,f.diff())," <f,f''>=",err2, " order=",exp2)
        if periodic:
            perio_grad=Field(m,periodic_gradient(f.base, m.get_s_with_periodic_endpoint(), axis=1))
            perio_err=np.sqrt(dot(f[0]+perio_grad[1],f[0]+perio_grad[1]))
            perio_exp=np.log2(operio_err/perio_err)
            operio_err=perio_err
            print("  "," periodic grad error:",perio_err,perio_exp)
        oerr=err
        oerr2=err2
        y = Field(m,n_comp=3,value=1)
        y_array = y.nodval()
        sol = solve_bvp(derivative_cos, bc_cos, m.get_s_with_periodic_endpoint(),
            y_array, max_nodes=10*N, verbose=0, tol=1e-4)
        soltol = solve_bvp(derivative_cos, bc_cos, m.get_s_with_periodic_endpoint(),
            y_array, max_nodes=10*N, verbose=0, tol=1e-3)
        if sol.status!=0 or sol.success!=True: print(sol.message)
        else:
            m = Mesh(s=sol.x, periodic=periodic)
            mtol = Mesh(s=soltol.x, periodic=periodic)
            y = Field(m, sol.y, { 'debug':True })
            ytol = Field(mtol, soltol.y, { 'debug':True })
            fm = f.to_mesh(m)
            fmtol = f.to_mesh(mtol)
            abs_fm=(np.abs(y-fm))
            err4tol=np.sqrt(dot(ytol-fmtol,ytol-fmtol))
            exp4tol=np.log2(oerr4tol/err4tol)
            err3tol=np.max(np.abs(ytol-fmtol).nodval(),axis=1)
            exp3tol=np.log2(oerr3tol/err3tol)
            err4=np.sqrt(dot(y-fm,y-fm))
            exp4=np.log2(oerr4/err4)
            err3=np.max(abs_fm.nodval(),axis=1)
            exp3=np.log2(oerr3/err3)
            print(" ODE tol 10^-4/8^N: N=", m.N," L² error:",err4,"order=",exp4," L∞ error:",err3,"order=",exp3)
            print(" ODE tol 10^-2: N=", mtol.N," L² error:",err4tol,"order=",exp4tol," L∞ error:",err3tol,"order=",exp3tol)
            oerr3tol=err3tol
            oerr4tol=err4tol
            oerr3=err3
            oerr4=err4
