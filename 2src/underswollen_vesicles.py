#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

# Compute the equilibrium shape of a 2D 'vesicle' 
#  - low surface extensibility compared to bending modulus, choose tolerance 
#    (indicative range, 1e-3 down to 1e-8) :
target_kappa=1e-3
#  - fixed area (2D volume), choose small axis of initial ellipse (indicative
#    range, 0.25 down to 0.125, self interpenetration for less)
small_radius=.35
# Pseudotime is used to reduce logarithmically kappa down to target. Frictional
# damping is used and reduced simultaneously to 0. There is no forcing of the
# position of the mass center/orientation, so at 0 friction any translation or
# rotation is admissible.
#
# For target_kappa<1e-5 small_radius=.12, analytical Jacobian improves speed by 30%.

from class_cortex import *
from common_utils import *
import time


plt.style.use('dark_background')
fig, ax= plt.subplots()

cortex_par=cortex_par_defaults()
#cortex_par["elastic_tolerance"]=2
cortex_par["target_area"]='initial'
cortex_par["boundary_conditions"]='periodic'
cortex_par["max_nodes"]=9000
#cx = Cortex({'shape':'circle', 'radius':1, 'center':(0,1+del0), 'N':1000}, cortex_par)
cx = Cortex({'shape':'ellipse', 'radii':(1,small_radius), 'angle':0, 'center':(0,1+del0), 'N':1000}, cortex_par)
#cx.add_external_forces(center_to_zero)

#cx.cortex_par['analytical_Jacobian'] = True

t0=time.process_time()
extra_iterations=int(30*np.log10(.2/small_radius))+0
print("Extra iterations: ",extra_iterations)
for kappa,damping in zip(np.logspace(-2,np.log10(target_kappa),21+extra_iterations),[*np.logspace(2+extra_iterations/5, -6, 20+extra_iterations),1e-6]):
    cx.cortex_par["kappa"]=kappa
    cx.cortex_par["frictional_damping"]=damping
    print(cx.cortex_par)
    res=cx.elastic_equilibrium()
    E_el = cx.elastic_energy()
    E_bg = cx.bending_energy()
    print("E_el = ", E_el, " E_bg = ", E_bg, " tot = ", E_bg+E_el)
    print("Δα=",cx.alpha().max()-cx.alpha().min(), "<x,y>", np.mean(cx.xy(),axis=0), "E_f = ", cx.friction_E, \
        "p=", cx.Lagrange_multipliers["values"][0], "A=", cx.enclosed_area()) 
    if res!=0: print("No convergence, exiting"); 
print("Calculation time",time.process_time()-t0)

longaxis=np.linalg.norm(cx.xy()[0]-cx.xy()[cx.N()//2])
shortaxis=np.linalg.norm(cx.xy()[cx.N()//4]-cx.xy()[3*cx.N()//4])
print("longaxis ", longaxis, ", shortaxis ", shortaxis) 

cortex_plot_par = cortex_plot_par_defaults()
cortex_plot_par['width']=9
cortex_plot_par['equal_axes']=True
cortex_plot=cx.plot(ax)
plt.plot([cx.x()[0],cx.x()[cx.N()//2]],[cx.y()[0],cx.y()[cx.N()//2]],"o")
plt.plot([cx.x()[cx.N()//4],cx.x()[3*cx.N()//4]],[cx.y()[cx.N()//4],cx.y()[3*cx.N()//4]],"o")
print("Long axis:",np.linalg.norm(cx.xy()[0]-cx.xy()[cx.N()//2]))
print("Short axis:",np.linalg.norm(cx.xy()[cx.N()//4]-cx.xy()[3*cx.N()//4]))

padding=max(np.max(cx.x())-np.min(cx.x()),np.max(cx.y())-np.min(cx.y()))*.05
ax.set_xlim(np.min(cx.x())-padding,np.max(cx.x())+padding)
ax.set_ylim(np.min(cx.y())-padding,np.max(cx.y())+padding)
#ax.set_ylim(-.1,np.max(cx.y())+padding)
ax.relim()
fig.colorbar(cortex_plot, label=cortex_plot_par['plot_field'])
plt.show()
