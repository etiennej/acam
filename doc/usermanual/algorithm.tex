% main: main.tex
\chapter{Algorithms}

\section{Introduction}

In this chapter, the ACAM algorithms are described.

\section{Types}

ACAM defines several classes which are designed to enrich \t{numpy} types with additional data.

\subsection{Class \t{\keyword{Mesh}} and \t{\keyword{field}}}

ACAM manages 1D fields, for which so there seems no reason to have complicated types. However, 
the subtelty is about periodic boundary conditions which are optional but need to be managed.
This has been the motivation to develop specific classes which are aware of these conditions
and can make the management of them instead of carrying extra data and code in each situation.

\paragraph{Nonperiodic case.}
Class \t{Mesh} can be initialised with \verb|Mesh(s=[|$s_0,...,s_{N-1}$\t{])},
an array of size \t{N} which contains 1D curvilinear coordinate $s_i$ of nodes on which a
field can be defined. 
Alternatively, it can be initialised with \verb|Mesh(N, s_range=(|$s_0,s_N$\t{))} in which case the $s_i$ will be calculated with \t{numpy.linspace}.
It has method \verb|get_s()| which returns 
It can return the size of elements in a table of size \t{N-1} with \verb|get_h()|, and
the average size of elements neighbouring node $i$ in a table of size \t{N}, 
$$
\verb|get_h_avg()| = \left( \frac{s_1-s_0}{2}, \frac{s_2-s_0}{2},..., \frac{s_{i+1}-s_{i-1}}{2}, ..., \frac{s_{N-1}-s_{N-3}}{2}, \frac{s_{N-1}-s_{N-2}}{2}  \right)
$$
\t{Field}s are then functions of $\big(L_2(s_0,s_N) \cap C^0(s_0,s_N)\big)^n$ which are piecewise linear on the segments $[s_i,s_{i+1}]$.
Class \t{Field} is derived from \t{numpy}'s \t{ndarray} and can be used directly as an array for most purposes.
However, as long as this remains consistent, it retains the characteristics of a \t{Field}, that is, it is
defined on a \t{Mesh}.
Multiple fields or nonscalar fields can be managed by \t{Field}, indeed the \t{ndarray} is of size \t{n}$\times$\t{Mesh.N},
where the size of the zeroth axis \t{n} is the number of components, an attribute of \t{Field}. A \t{Field} can be sliced along this axis and remain
a \t{Field}. 
\t{Field} initialisation requires a mesh and either the number of components \t{n} (in which case the field is created of size \t{n}$\times$\t{Mesh.N}
filled with a default or user-defined value) or an existing array (or field) from whose zeroth axis \t{n} will be extracted, and whose first
axis must be of size \t{Mesh.N}.
Alternatively, the method \verb|interpolate(mesh, f_e)| also construct a field with $\t{n} = \verb|len(f_e)|$ components (or $\t{n} = 1$ if \verb|f_e|
is not a list) whose nodal values are the evaluation of \verb|f_e[i]| on $s_i$.

A \t{Field} \t{f} calculates (and stores) an interpolant which allows to evaluate it at any set of nongrid points \verb|new_s| simply with \verb|f(new_s)|.
A new \t{Field} can be constructed on a new mesh using \verb|g=f.to_mesh(m2)|.
It also calculates (and stores) its own first and second order finite differences, which can be obtained as arrays of nodal values with
\verb|nodval_diff()| and \verb|nodval_ddiff()| or as \t{Field}s with \t{diff()} and \t{ddiff()}.

The internal product of $L_2(s_0,s_N)$ can be computed for any two fields \t{f,g} defined on the same \t{Mesh} using \t{dot(f,g)},
$$
\t{dot(f,g)} = \int_{s_0}^{s_N} \t{f}(s)\t{g}(s) \mathrm{d}s.
$$

\paragraph{Periodic case.} 
A periodic \t{Mesh} is created by adding \t{periodic=True} to its initialiser. Then \t{N} is understood as the number of \emph{independent}
nodes, thus there is a node of coordinate $s_{N}$ which is identified with node $s_0$. 
The method \verb|get_s()| does \emph{not} return $s_N$, thus still returning an array of size \t{N}, and the method
\verb|get_s_with_periodic_endpoint()| returns $s_0,...s_N$. The method \verb|get_h()| now returns $(h_i = s_{i+1}-s_i)_{i=0,..N-1}$ and
\verb|get_h_avg()| returns $\left(\frac{h_{i-1 \mathrm{mod} N}+h_i}{2}\right)_{i=0,..N-1}$.

A \t{Field} created on a periodic mesh is periodic. \t{ACAM} allows a \keyword{periodicity offset} between the value at $s_0$ and the value 
at $s_N$. This is useful e.g.\ to count a number of loops $k$: if a \t{Field} interpolates $\theta(s)$ the angle that a closed curve of the plane makes
with a basis vector, then $\theta(s_N) = \theta(s_0) + 2k\pi$, where e.g.\ $k=1$ corresponds to a non-self interesecting closed curve oriented in the 
direct sense. The \verb|periodicity_offset| is zero by default, it is calculated from the data if data is given or can be specified.

\subsection{Class \texttt{\lstinline|Sparse_Multifield|}}

The joint density of adhesions $\Pi(s_i \in \C_i, s_j \in \C_j)$ is a 2D field defined on $(\bigcup_i \C_i)^2$. 
It is expected to have a compact support much smaller than $(\bigcup_i \C_i)^2$, so its discretisation 
$\mathtt{\Pi}$ can
be efficiently represented by a collection of pairs \verb|(s_i, s_j)| and the values $\mathtt{\Pi}\verb|(s_i, s_j)|$
associated to them, rather than a full array on $(\bigcup_i \C_i)^2$.
This is reminiscent of the data structure of sparse matrices. In addition, many operations on $\Pi$ can be written as:
$$
f(s_i) = \int_{\C_j} \Pi(s_i,s_j) g(s_j) \mathrm{d}s_j
$$
Representing $\mathtt{\Pi}$ as a matrix is convenient since we can then calculate the discrete \t{f} from the discrete \t{g} as:
$$
\t{f} = \mathtt{\Pi} \cdot \t{g}.
$$



\section{Implementation of ACAM}

\subsection{Class \t{Cortex}}

This class implements a morphoelastic rod in the plane as described in \cite{NestorBergmann+Sanson.2022.1}, but with a different
relaxation algorighm. Indeed, compared to that paper, the viscoelastic relaxation is not taken into account by modifying
the `initial' configuration but rather the prestrain field $\gamma$ following the ideas of \cite{Jallon+Etienne.2024.1}.

\subsubsection{Data}

The data stored in this class is as follows:
\begin{itemize}
\item a 1D \t{mesh}, corresponding to the curvilinear coordinate $\hat{S} \in [0,\hat{L}]$ of the \initial\ configuration,
\item the prestrain \t{Field}s \t{gamma} \JE{viscoel},
\item the parametric representation $\mathtt{\Psi}$ of the rod, whose components are the $x(\Shat)$ and $y(\Shat)$ coordinates of the
rod in its current configuration, the angle $\theta(\Shat)$ of its tangent in $\Shat$ with $\vc{e}_x$, the derivatives
$\DShat\theta$ and $\DShat^2\theta$, and the local elastic stretch $\alpha(\Shat)$.
\end{itemize}

Additionally, the class also stores the \t{Field} \verb|memorized_|$\mathtt{\Psi}$ which is the parametric representation
$\mathtt{\Psi}$ before elastic equilibrium has been (re)calculated. This allows e.g.\ to compute time derivatives.

\subsubsection{Curvilinear coordinates and meshes}

Three configurations have to be considered, which are parametrised by different curvilinear coordinates:
\begin{itemize}
\item the initial configuration coordinate $\hat{S} \in [0,\hat{L}]$, which is unaffected by any deformation and is thus constant;
\item the stress-free virtual configuration coordinate $S \in [0,L]$, which is related to the initial configuration by the prestrain
$\gamma$ through the relation $\DShat S = \gamma$;
\item the current configuration coordinate $s \in [0,l]$, which is related to the stress-free virtual configuration coordinate
by the elastic stretch through the relation $\DS s = \alpha$, thus defining the total stretch relative to the initial configuration
$\lambda = \gamma \alpha = \DShat s$.
\end{itemize}
The elasticity problem is in terms of $S$ but is mapped onto $\Shat$ \cite{NestorBergmann+Sanson.2022.1},
thus, in the code, only $\Shat$ needs to be explicitly represented.

The class \t{Cortex} must however handle several representations of it, since mesh adaptation is useful to solve the ealsticity problem.
A \t{Cortex} instance representing a viscoelastic rod at time $t$ thus has a $\t{mesh}(t) = \{ s_0=0, s_1, ..., s_{N_{\star}}=\hat{L} \}$, 
where $N_{\star} = N+1$ if the cortex is a closed loop (periodic mesh)
and $N_{\star}=N$ else. During iterative solving, other points $\{ s'_0, s'_1, ..., s'_{N'}\}$ of $[0,\hat{L}]$ can be used to evaluate derivatives
of the current solution guess and the external forces, they are referred to as \t{grid}s in the code. 
After solving, the approximate solution is returned on a new $\t{mesh} (t+\Delta t):= \{ s''_0=0, s''_1, ..., s''_{N''_{\star}}=\hat{L} \}$.

\subsubsection{Constraints}

Lagrange multipliers are used to impose constraints with the shape:
$$
\mathcal{G}_k = \intd{\c_j}{}{ \tilde{g}_k(s_j) }{s_j} = G_k
$$
for constraints $\tilde{g}_k$, $k\in\{0,..,K-1\}$. E.g., an area constraint can be written
using $\tilde{g}_a = x\ded{y}{s} - y\ded{x}{s} = -\pt{r}\cdot\vc{n}$ and $G_a$ the target area.
Changing variable, $g_k(\Shat(s)) = \tilde{g}_k(s)$,
$$
\mathcal{G}_k = \intd{\Chat_j}{}{ {g}_k(S_j) \lambda }{\Shat_j} = G_k.
$$

The Lagrangian thus writes:
$$
\mathcal{L}(\pt{u};\pt{\mu}) = \frac{\delta\mathcal{U}}{\delta\pt{u}} + \sum_k\mu_k\frac{\delta\mathcal{G}_k}{\delta\pt{u}}
$$

on the problem with the shape:
\begin{align*}
\mathbf{n}' +\mathbf{n}_{\text{ext}} + \sum_k \mu_k \DShat g_k = 0
\end{align*}
