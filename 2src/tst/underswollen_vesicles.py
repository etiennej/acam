#!/usr/bin/env python3
# main conda acam
# -*- coding: utf-8 -*-
# =============================================================================
# Author : Jocelyn Etienne
# (CC) october 2022
# =============================================================================

"""
Test case
---
Will validate:
* non-regression of beam model
* non-regression of periodic boundary condition
* non-regression of analytic Jacobian
* non-regression of area conservation
"""

import sys,os
# allow to find classes in the parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from class_cortex import *
from common_utils import *
import time
import datetime
try:
    import cpuinfo
    mycpuinfo = cpuinfo.get_cpu_info()['brand_raw']
except:
    mycpuinfo = "Unknown CPU -- install py-cpuinfo to record it"


# Tolerance of non-regression
tol=1e-3
# Set to False to run test.
# Set to True to regenerate non-regression test results. /!\
build_test=False
if build_test:
    yes = input("Confirm recalculation of nonregression test results. yes/[no]:")
    if yes!='yes':
        print("Running as a test, remember to turn build_test=False in the code")
        build_test=False

# Compute the equilibrium shape of a 2D 'vesicle' 
#  - low surface extensibility compared to bending modulus, choose tolerance 
#    (indicative range, 1e-3 down to 1e-8) :
target_kappa_list=[1e-3,1e-6]
#  - fixed area (2D volume), choose small axis of initial ellipse (indicative
#    range, 0.25 down to 0.125, self interpenetration for less)
small_radius_list=[.35,.3,.25]
# Pseudotime is used to reduce logarithmically kappa down to target. Frictional
# damping is used and reduced simultaneously to 0. There is no forcing of the
# position of the mass center/orientation, so at 0 friction any translation or
# rotation is admissible.
#
# For target_kappa<1e-5 small_radius=.12, analytical Jacobian improves speed by 30%.

if build_test:
    cases = []
    for kappa in target_kappa_list:
     for small_radius in small_radius_list:
      for analytical_Jacobian in [True,False]:
            cases += [ { 'kappa':kappa, 'small_radius':small_radius, 'analytical_Jacobian':analytical_Jacobian,
                         'cpu': mycpuinfo, 'date':str(datetime.datetime.today()) } ]
else:
    cases = np.load("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], allow_pickle=True)

success=0
perf=[]
for case in cases:
    print (case)
    try:
        target_kappa = case['kappa']
        small_radius = case['small_radius']
        cortex_par=cortex_par_defaults()
        cortex_par["elastic_tolerance"]=tol/10
        cortex_par["verbose"]=False
        cortex_par["boundary_conditions"]='periodic'
        cortex_par["target_area"]='initial'
        cx = Cortex({'shape':'ellipse', 'radii':(1,small_radius), 'angle':0, 'center':(0,1+del0), 'N':1000}, cortex_par)
        cx.cortex_par['analytical_Jacobian'] = case['analytical_Jacobian']
        ini_contourlength=np.trapz(cx.alpha(),cx.mesh._s)
        ini_area=cx.enclosed_area()

        t0=time.process_time()
        extra_iterations=int(30*np.log10(.2/small_radius))
        for kappa,damping in zip(np.logspace(-2,np.log10(target_kappa),21+extra_iterations),[*np.logspace(2+extra_iterations/5, -6, 20+extra_iterations),1e-6]):
            cx.cortex_par["kappa"]=kappa
            cx.cortex_par["frictional_damping"]=damping
            res=cx.elastic_equilibrium()
            E_el = cx.elastic_energy()
            E_bg = cx.bending_energy()
            if res!=0: break
        success_status = res 
        if res==0:
            cputime=time.process_time()-t0
            contourlength=np.trapz(cx.alpha(),cx.mesh._s)
            longaxis=np.linalg.norm(cx.xy()[0]-cx.xy()[cx.N()//2])
            shortaxis=np.linalg.norm(cx.xy()[cx.N()//4]-cx.xy()[3*cx.N()//4])
    except Exception as e: 
        print("Error ",e," for case ",case)
        res=1
    if build_test:
        if res==0:
            case['longaxis']=longaxis
            case['shortaxis']=shortaxis
            case['contourlength']=contourlength
            case['area']=cx.enclosed_area()
            case['cputime']=cputime
            case['success']=True
        else:
            case['success']=False
        print(case)
    else:
        passed=True
        if case['success']:
            if res>0:
                print ("Not converging when it used to.\n",case)
                passed=False
            else:
                perf+=[ cputime/case['cputime'] ]
                if abs(case['longaxis']-longaxis)/longaxis > tol \
                or abs(case['shortaxis']-shortaxis)/shortaxis > tol \
                or abs(case['contourlength']-contourlength)/contourlength > tol \
                or abs(case['area']-cx.enclosed_area())/cx.enclosed_area() > tol:
                    print ("Not within tolerance.\nRecorded:",case,
                        "\nPresent:",{'area':cx.enclosed_area(),'contourlength':contourlength,
                            'longaxis':longaxis,'shortaxis':shortaxis},
                        "\nRatios:",(case['longaxis']-longaxis)/longaxis,(case['shortaxis']-shortaxis)/shortaxis,
                        "\nInitial:",{'area':ini_area,'contourlength':ini_contourlength})
                    passed=False
            success+=passed

if build_test:
    np.save("tst_%s.npy" % os.path.basename(__file__).rsplit(".py")[0], cases)
else:
    target = sum([case['success'] for case in cases])
    print("Could run %d tests out of %d, of which %d successful and median relative performance %f"
        % (len(perf), target, success, np.median(perf)))
    if success<target:
       print("Not all tests succeeded, marked as failed")
       exit(1)
    if np.median(perf)>1.1:
       print("Performance unexpectedly poor, marked as failed")
       exit(1)
