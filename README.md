# ACAM: an **A**pposed-**C**ortex **A**dhesion **M**odel of an epithelial tissue.

![AppCoM](docs/Figures/simulation.gif)

<hr/>

[![Doc Status](https://readthedocs.org/projects/appcom/badge/?version=latest)](https://appcom.readthedocs.io/en/latest/)

The `ACAM` library is an implementation of a mechanical model of an active epithelial tissue, see [our _PLoS Comput Biol_ paper on the biophysical modelling and results](https://www.doi.org/10.1371/journal.pcbi.1009812). 

As of Feb 2025, version 2 is currently under development, with improvements on the viscoelastic model (based on our [_J Elas_ 2025 paper](https://liphy-annuaire.univ-grenoble-alpes.fr/pages_personnelles/jocelyn_etienne/papers.html#Jallon+Etienne.2025.1)) and of the adhesion model.

## Overview

### The apposed-cortex adhesion model

#### How is the cell cortex represented?

Each cell cortex in ACAM is represented as an active, continuum morphoelastic rod with resistance to bending and extension.  By explicitly considering both cortices along bicellular junctions, the model is able to replicate important cell behaviours that are not captured in many existing models e.g. cell-cell shearing and material flow around cell vertices.

#### How are adhesions represented?

Adhesions are modelled as simple springs, explicitly coupling neighbouring cell cortices.  Adhesion molecules are given a characteristic timescale, representing the average time between binding and unbinding, which modules tissue dynamics.

![AppCoM](docs/Figures/model.png)

### Demo: loading and viewing a tissue

```python
import dill
import matplotlib.pyplot as plt

# Load a stored tissue, with 14 cells
with open('pickled_tissues/14_cells', 'rb') as new_tissue:
        eptm = dill.load(new_tissue)

# Pass the adhesion data to the cells
eptm.update_adhesion_points_between_all_cortices()
# Add some prestress to the junction shared by cells A and B
prestrech_magnitude = 1 - 0.01
eptm.apply_prestretch_to_cell_identity_pairs(prestrech_magnitude, ['A','B'])

# View in matplotlib
fig, ax = plt.subplots(figsize=(11, 9))
eptm.plot_self(ax=ax, plot_stress=True, plot_tension=True)
plt.show()
```

### Documentation

* The documentation is browsable online [here](https://appcom.readthedocs.io/en/latest/)

### Authors

* Alexander Nestor-Bergmann (initial design, first developer) - University of Cambridge
* Alexander Fletcher - University of Sheffield
* Guy Blanchard - University of Cambridge
* Jocelyn Étienne (initial design, current maintainer/developer) - Université Grenoble Alpes

### How to cite

Alexander Nestor-Bergmann, Guy B Blanchard, Alexander Fletcher, & Jocelyn Etienne. (2022). ACAM - Apposed Cortex Adhesion Model (1.0.0). Zenodo. https://doi.org/10.5281/zenodo.5838249

## Dependencies

- Python 3.x
- dill
- joblib
- matplotlib
- more-itertools
- numpy
- scikit-learn
- scipy
- Shapely

LaTeX installation should include `type1ec.sty` provided by package `cm-super`.
